package com.Unique.Squad.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class SessionExpiredException extends RuntimeException {
    public SessionExpiredException(String sessionExpired) {
        super(sessionExpired);
    }
}
