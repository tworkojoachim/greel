package com.Unique.Squad.security;

import com.Unique.Squad.exceptions.AccountNotConfirmedException;
import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.jwt.AuthResponse;
import com.Unique.Squad.jwt.JwtResponse;
import com.Unique.Squad.jwt.JwtUtils;
import com.Unique.Squad.jwt.RefreshToken;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserDto;
import com.Unique.Squad.user.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final UserRepository userRepository;
    private final RefreshToken refreshToken;

    public AuthService(AuthenticationManager authenticationManager, JwtUtils jwtUtils, UserRepository userRepository, RefreshToken refreshToken) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.userRepository = userRepository;
        this.refreshToken = refreshToken;
    }

    public AuthResponse authenticateUser(UserDto userDto) {

        Optional<User> user = userRepository.findByUsername(userDto.getUsername());
        if(user.isPresent()){
            if(user.get().getStatus().equals(User.Status.NOT_CONFIRMED)) {
                throw new AccountNotConfirmedException("Account not confirmed, \n please confirm email!");
            }
        }
        else
            throw new BadRequestException("Invalid credentials");

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userDto.getUsername(),
                        userDto.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtUtils.generateToken(userDto);
        String rToken = refreshToken.createRefreshToken(user.get());
        user.get().setToken(jwt);
        userRepository.save(user.get());
        log.info("User " + userDto.getUsername() + " has logged in.");
        return new AuthResponse(jwt, rToken);
    }

    public JwtResponse logout(String token) {
        log.info("User logout start");
        Optional<User> user = userRepository.findByToken(token);
        if (user.isPresent()) {
            log.info("User " + token + " exists in database");
            user.get().setToken(null);
            user.get().setRefreshToken(null);
            userRepository.save(user.get());
            SecurityContextHolder.clearContext();
            log.info("User " + token + " successfully logout");
            return new JwtResponse("Logout successfully!");
        }
        else throw new BadRequestException("Something went wrong!");
    }
}
