package com.Unique.Squad.userProfile;

public class UserProfileConstant {

    public final static String FIRST_NAME_VALID = "First name must have min 2 and max 25 character!";
    public final static String LAST_NAME_VALID = "Last name must have min 2 and max 25 character!";
    public final static String PHONE_NUMBER_VALID = "Phone number must have 9 numbers!";
}
