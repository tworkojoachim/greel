package com.Unique.Squad.userProfile;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserProfileDto {

    String firstName;
    String lastName;
    String phoneNumber;
    String gitHub;
    String linkedin;
    UserProfile.Position position;
    UserProfile.SocialStatus socialStatus;
    Long experience;

    public UserProfileDto(String firstName, String lastName, String phoneNumber, String gitHub, String linkedin, UserProfile.Position position, UserProfile.SocialStatus socialStatus, Long experience) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.gitHub = gitHub;
        this.linkedin = linkedin;
        this.position = position;
        this.socialStatus = socialStatus;
        this.experience = experience;
    }
}
