package com.Unique.Squad.user;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoadUserService implements UserDetailsService {

    private final UserRepository userRepository;

    public LoadUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> user = userRepository.findByUsername(username);
        if(user.isPresent()){
            return UserDto.create(user.get());
        }
        else throw new UsernameNotFoundException("Username "+username+" not found!");
    }
}

