package com.Unique.Squad.application;

import com.Unique.Squad.team.Team;
import com.Unique.Squad.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    Long id;

    String message;
    LocalDateTime date;
    boolean userInvite;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    User user;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    Team team;
}
