package com.Unique.Squad.jwt;

import com.Unique.Squad.exceptions.SessionExpiredException;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserDto;
import com.Unique.Squad.user.UserRepository;
import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;


import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

import static com.Unique.Squad.constant.JWTConstant.*;

@Log4j2
@Component
public class JwtUtils implements Serializable {

    private final UserRepository userRepository;

    public JwtUtils(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // generate token
    public String generateToken(UserDto userDto) {

        String username = userDto.getUsername();
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + TOKEN_EXPIRATION);

        return Jwts.builder()
                .setSubject(username)
                .setIssuer(TOKEN_ISSUER)
                .setExpiration(expiryDate)
                .setAudience(TOKEN_AUDIENCE)
                .signWith(keyy)
                .setHeaderParam("type", TOKEN_TYPE)
                . claim("role", userDto.getAuthorities())
                .compact();
    }

    // get username from token
    public String getUsernameFromJWT(String token) {
            Claims claims = Jwts.parser()
                    .setSigningKey(keyy)
                    .parseClaimsJws(token)
                    .getBody();
            return claims.getSubject();
    }

    private String prepareToken(String token) {
        if (token.startsWith("Bearer") && !token.isBlank()) {
            return token.replace("Bearer ", "");
        }
        return null;
    }

    // validation token
    public boolean validateToken(String authToken) {
        if(userRepository.existsByToken(authToken)) {
            try {
                String token = prepareToken(authToken);
                Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(token);
                return true;
            } catch (MalformedJwtException ex) {
                log.error("Invalid JWT token");
            } catch (ExpiredJwtException ex) {
                Optional<User> user = userRepository.findByToken(authToken);
                if (user.isPresent()) {
                    user.get().setToken(null);
                    userRepository.save(user.get());
                    System.out.println("Usunięto token ! -------------------------------------------------------------------");
                }
                log.error("Expired JWT token");
            } catch (UnsupportedJwtException ex) {
                log.error("Unsupported JWT token");
            } catch (IllegalArgumentException ex) {
                log.error("JWT claims string is empty.");
            }
            return false;
        }
        else throw new SessionExpiredException("The session has expired");
    }

}
