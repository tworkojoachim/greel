package com.Unique.Squad.user;

import com.Unique.Squad.application.Application;
import com.Unique.Squad.role.Role;
import com.Unique.Squad.skills.userSkills.UserSkills;
import com.Unique.Squad.userProfile.UserProfile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "APP_USER", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "USERNAME"
        }),
        @UniqueConstraint(columnNames = {
                "EMAIL"
        }),
})
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    Long id;

    @Column(name = "USERNAME", unique = true, nullable = false)
    String username;
    @Column(name = "PASSWORD", nullable = false)
    String password;
    @Column(name = "EMAIL", unique = true, nullable = false)
    String email;
    @Column(name = "TOKEN", unique = true, nullable = true)
    String token;
    @Column(name = "REFRESH_TOKEN", unique = true, nullable = true)
    String refreshToken;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", length = 60)
    Status status;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "USER_ROLE",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    Set<Role> roles;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    Set<UserSkills> skills;

    @JsonIgnore
    @OneToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(
            name = "USER_PROFILE_ID",
            referencedColumnName = "ID")
    UserProfile userProfile;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    Set<Application> applications;

    public User(String username, String encode) {
        this.username = username;
        this.password = encode;
    }

    public static enum Status {ACTIVE, INACTIVE, NOT_CONFIRMED;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email) &&
                status == user.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, email, status);
    }
}
