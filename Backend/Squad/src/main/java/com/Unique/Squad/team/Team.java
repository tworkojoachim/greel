package com.Unique.Squad.team;

import com.Unique.Squad.application.Application;
import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.teamSkills.TeamSkills;
import com.Unique.Squad.user.User;
import com.Unique.Squad.userProfile.UserProfile;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    Long id;

    String name;
    String description;
    String projectDescription;
    String peopleSought;
    Long teamSize;
    LocalDate dateOfCreation;

    @JsonIgnore
    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.REFRESH,
            orphanRemoval = true)
    @JoinTable(name = "team_admin",
            joinColumns = {
                    @JoinColumn(name = "TeamId")},
            inverseJoinColumns = {
                    @JoinColumn(name = "AdminId")}
    )
    Set<User> userAdmins;

    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.REFRESH,
            orphanRemoval = true)
    @JoinTable(name = "team_user",
            joinColumns = {
                @JoinColumn(name = "TeamId")},
            inverseJoinColumns = {
                @JoinColumn(name = "UserId")}
    )
    Set<User> users;


    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "team",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    Set<Application> applications;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "team",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    Set<TeamSkills> teamSkills;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(id, team.id) &&
                Objects.equals(name, team.name) &&
                Objects.equals(description, team.description) &&
                Objects.equals(projectDescription, team.projectDescription) &&
                Objects.equals(peopleSought, team.peopleSought) &&
                Objects.equals(teamSize, team.teamSize) &&
                Objects.equals(dateOfCreation, team.dateOfCreation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, projectDescription, peopleSought, teamSize, dateOfCreation);
    }
}
