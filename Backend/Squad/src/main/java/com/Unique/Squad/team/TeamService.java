package com.Unique.Squad.team;

import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserInfoDto;

import java.time.LocalDate;
import java.util.Set;

public interface TeamService {

    Team getTeam(Long id);
    TeamInfoDto getTeamInfoById(Long id);
    Team getTeamByUser(String username);
    User getUserFromDb(String username);
    LocalDate format(LocalDate localDate);
    boolean existsByUser(String username);
    TeamInfoDto getTeamInfoByUsername(String username);
    Team addTeam(Team team, String username);
    boolean existsByUserAdmin(String username);
    Set<UserInfoDto> getAllTeamUsers(String username);
    void givePermissions(Long userId, String username);
    void removePermissions(Long userId, String username);
    void removeUserFromTeam(Long userId, String username);
}
