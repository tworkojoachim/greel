package com.Unique.Squad.userProfile;

import com.Unique.Squad.user.User;

import java.util.Optional;

public interface UserProfileService {

    UserProfileDto getUserProfile(String username);
    UserProfileDto getUserProfileById(Long id);
    UserProfile save(UserProfile userProfile, String username);
}
