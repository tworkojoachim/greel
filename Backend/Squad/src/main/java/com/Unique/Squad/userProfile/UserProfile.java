package com.Unique.Squad.userProfile;

import com.Unique.Squad.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Objects;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    Long id;

    String firstName;
    String lastName;
    String phoneNumber;
    String gitHub;
    String linkedin;
    Long experience;

    @Enumerated(EnumType.STRING)
    @Column(name = "POSITION", length = 30)
    Position position;

    @Enumerated(EnumType.STRING)
    @Column(name = "SOCIAL_STATUS", length = 30)
    SocialStatus socialStatus;

    @JsonIgnore
    @OneToOne(mappedBy = "userProfile")
    User user;

    public static enum Position {DEFAULT, BACKEND, FRONTEND, FULL_STACK ;}
    public static enum SocialStatus {DEFAULT, STUDENT, LOOKING_FOR_A_JOB, JOB_CHANGE ;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProfile that = (UserProfile) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(gitHub, that.gitHub) &&
                Objects.equals(linkedin, that.linkedin) &&
                Objects.equals(experience, that.experience) &&
                position == that.position &&
                socialStatus == that.socialStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, phoneNumber, gitHub, linkedin, experience, position, socialStatus);
    }

    public UserProfile(Long id, String firstName, String gitHub, String linkedin, Long experience, Position position, SocialStatus socialStatus) {
        this.id = id;
        this.firstName = firstName;
        this.gitHub = gitHub;
        this.linkedin = linkedin;
        this.experience = experience;
        this.position = position;
        this.socialStatus = socialStatus;
    }
}
