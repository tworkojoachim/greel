package com.Unique.Squad.skills.teamSkills;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SearchModel {

    Long sizeTeam;
    List<String> needsList;
    String category;
}
