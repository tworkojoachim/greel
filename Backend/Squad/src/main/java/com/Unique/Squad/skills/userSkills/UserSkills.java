package com.Unique.Squad.skills.userSkills;

import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserSkills extends Skills {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    User user;

    public UserSkills(User user) {
        super();
        this.user = user;
    }
}

