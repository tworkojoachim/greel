package com.Unique.Squad.skills.userSkills;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.FieldIsEmptyException;
import com.Unique.Squad.exceptions.SkillsAlreadyExistsException;
import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserSkillsServiceImpl implements UserSkillsService {

    final UserSkillsRepository userSkillsRepository;
    final UserRepository userRepository;

    public UserSkillsServiceImpl(UserSkillsRepository userSkillsRepository, UserRepository userRepository) {
        this.userSkillsRepository = userSkillsRepository;
        this.userRepository = userRepository;
    }

    @Override
    public UserSkills addUserSkills(UserSkills userSkills, String username) {
        if(userSkills.getName() == null || userSkills.getNumberOfMonth() == null || userSkills.getLevel() == null)
            throw new FieldIsEmptyException("The fields cannot be empty");
        Optional<User> user = userRepository.findByUsername(username);

        if(user.isPresent()){
            for (UserSkills value: user.get().getSkills()) {
                if(value.getName().equals(userSkills.getName()))
                    throw new SkillsAlreadyExistsException("You already have this skill!");
            }

            userSkills.setUser(user.get());
            return userSkillsRepository.save(userSkills);
        }
        throw new UsernameNotFoundException("User not found");
    }

    @Override
    public List<SkillsDto> getAllUserSkills(String username) {

        Optional<User> user = userRepository.findByUsername(username);
        if(user.isPresent()){
            return userSkillsRepository.findAllByUser(user.get());
        }
        else throw new UsernameNotFoundException("Username" + username + " not exists!");
    }

    @Override
    public List<SkillsDto> getAllUserSkillsById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()){
            return userSkillsRepository.findAllByUser(user.get());
        }
        else throw new BadRequestException("User skills not found!");
    }

    @Override
    public void deleteUserSkill(Long id) {
        userSkillsRepository.deleteById(id);
    }

}
