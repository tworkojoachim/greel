package com.Unique.Squad.user;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.FieldIsEmptyException;
import com.Unique.Squad.exceptions.NameAlreadyExistsException;
import com.Unique.Squad.exceptions.PasswordException;
import com.Unique.Squad.jwt.JwtResponse;
import com.Unique.Squad.registration.RegistrationModel;
import com.Unique.Squad.role.Role;
import com.Unique.Squad.role.RoleRepository;
import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.teamSkills.SearchModel;
import com.Unique.Squad.skills.teamSkills.TeamSkillsService;
import com.Unique.Squad.skills.teamSkills.TeamSkillsServiceImpl;
import com.Unique.Squad.skills.userSkills.SearchPeopleModel;
import com.Unique.Squad.userProfile.UserProfile;
import com.Unique.Squad.userProfile.UserProfileRepository;
import com.Unique.Squad.validation.PasswordValidation;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserServiceImpl implements UserService {

    final UserRepository userRepository;
    final RoleRepository roleRepository;
    final UserProfileRepository userProfileRepository;
    final BCryptPasswordEncoder encoder;
    final PasswordValidation passwordValidation;
    final TeamSkillsServiceImpl teamSkillsService;

    @Override
    public void registration(RegistrationModel model) {

        validation(model);
        UserProfile userProfile = new UserProfile();

        User user = User.builder()
                .username(model.getUsername())
                .password(encoder.encode(model.getPassword()))
                .email(model.getEmail())
                .roles(Collections.singleton(roleRepository.findByName(Role.Name.USER)))
                .status(User.Status.ACTIVE)
                .userProfile(userProfile)
                .build();

        userProfile.setUser(user);
        userRepository.save(user);
        userProfileRepository.save(userProfile);
    }

    @Override
    public void userValidationByUsername(String username) {
        if(username.isEmpty())
            throw new FieldIsEmptyException("Username is empty");

        if(userRepository.findByUsername(username).isPresent())
            throw new NameAlreadyExistsException("Username already exists");
    }

    @Override
    public void userValidationByEmail(String email) {
        if(email.isEmpty())
            throw new FieldIsEmptyException("Email is empty");

        if(userRepository.findByEmail(email).isPresent())
            throw new NameAlreadyExistsException("Email already exists");
    }

    @Override
    public void validation(RegistrationModel model) {
        userValidationByUsername(model.getUsername());
        userValidationByEmail(model.getEmail());
        if(!passwordValidation.isValid(model.getPassword(), model.getConfirmPassword()))
            throw new PasswordException("Password must contain: 8-30 characters, 1 upper case, 1 digital character, 1 special character, no white space");
    }

    @Override
    public List<UserProfile> searchAllUsersProfileBySkillsAndExperience(SearchPeopleModel searchModel, String username) {
        validSearchModel(searchModel);
        return userRepository.getAllSearchUsersProfile(
                searchModel.getExperience(),
                searchModel.getNeedsList(),
                teamSkillsService.categoryForm(searchModel.getCategory()),
                username);
    }

    private void validSearchModel(SearchPeopleModel searchModel) {
        if(searchModel.getNeedsList() == null || searchModel.getCategory() == null || searchModel.getExperience() == null)
            throw new FieldIsEmptyException("You must choose all options");
        if(searchModel.getExperience() > 35 || searchModel.getExperience() < 0)
            throw new BadRequestException("Experience must be between 0 and 35");
    }

}














