package com.Unique.Squad.constant;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;

public class JWTConstant {

    public static final SecretKey keyy = Keys.secretKeyFor(SignatureAlgorithm.HS256); //or HS384 or HS512
    private static final String base64Key = Encoders.BASE64.encode(keyy.getEncoded());

    public static final String JWT_SECRET = base64Key;

    public static final String AUTH_LOGIN_URL = "/authenticate";

    // JWT token defaults
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";
    public static final int TOKEN_EXPIRATION = 900000; // 15 minutes = 900 s

    private JWTConstant() {
        throw new IllegalStateException("Cannot create instance of static util class");
    }
}
