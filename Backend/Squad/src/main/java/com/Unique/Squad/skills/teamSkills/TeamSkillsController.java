package com.Unique.Squad.skills.teamSkills;

import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.skills.userSkills.UserSkills;
import com.Unique.Squad.team.Team;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/team")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TeamSkillsController {

    final TeamSkillsService teamSkillsService;

    @PostMapping("/search")
    public ResponseEntity<List<TeamSearchModel>> getSearchTeams(@RequestBody SearchModel searchModel) {
        return ResponseEntity.ok().body(teamSkillsService.getAllSearchTeams(searchModel));
    }

    @PostMapping("/skill")
    public ResponseEntity<TeamSkills> addSkill(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody TeamSkills teamSkills) {
        return ResponseEntity.ok().body(teamSkillsService.addSkill(teamSkills, user.getPrincipal().toString()));
    }

    @GetMapping("/skills")
    public ResponseEntity<List<SkillsDto>> getAllTeamSkills(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(teamSkillsService.getAllByTeam(user.getPrincipal().toString()));
    }

    @GetMapping("/skills/{id}")
    public ResponseEntity<Set<SkillsDto>> getAllTeamSkillsById(@PathVariable Long id) {
        return ResponseEntity.ok().body(teamSkillsService.getAllByTeamId(id));
    }

    @DeleteMapping("/skills/{id}")
    public HttpStatus deleteSkill(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @PathVariable Long id) {
        teamSkillsService.deleteSkill(id);
        return HttpStatus.OK;
    }
}
