package com.Unique.Squad.skills.teamSkills;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.FieldIsEmptyException;
import com.Unique.Squad.exceptions.NoTeamExceptions;
import com.Unique.Squad.exceptions.SkillsAlreadyExistsException;
import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.team.Team;
import com.Unique.Squad.team.TeamRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamSkillsServiceImpl implements TeamSkillsService{

    final TeamSkillsRepository teamSkillsRepository;
    final TeamRepository teamRepository;

    @Override
    public TeamSkills addSkill(TeamSkills teamSkills, String username) {
        if(teamSkills.getName() == null || teamSkills.getLevel() == null)
            throw new FieldIsEmptyException("The fields cannot be empty");

            Team team = findTeamByUsername(username);

            for (TeamSkills value: team.getTeamSkills()) {
                if (value.getName().equals(teamSkills.getName()))
                    throw new SkillsAlreadyExistsException("Team already have this required skill!");
            }

            teamSkills.setNumberOfMonth(0L);
            teamSkills.setTeam(team);
            return teamSkillsRepository.save(teamSkills);

    }

    @Override
    public List<SkillsDto> getAllByTeam(String username) {
        Team team = findTeamByUsername(username);
        return teamSkillsRepository.findAllByTeam(team);
    }

    @Override
    public Set<SkillsDto> getAllByTeamId(Long id) {
        return teamSkillsRepository.findAllByTeamId(id);
    }

    @Override
    public void deleteSkill(Long id) {
        teamSkillsRepository.deleteById(id);
    }

    @Override
    public Team findTeamByUsername(String username) {
        Optional<Team> team =  teamRepository.findTeamByUserUsername(username);
        if(team.isPresent()) return team.get();
        else  throw new NoTeamExceptions("Team no exists!");
    }

    @Override
    public List<TeamSearchModel> getAllSearchTeams(SearchModel searchModel) {
        validSearchModel(searchModel);
        return teamRepository.getAllSearchTeams(searchModel.getSizeTeam() , searchModel.getNeedsList(),  categoryForm(searchModel.getCategory()));
    }

    public List<Skills.Category> categoryForm(String category) {
        switch (category) {
            case "FRONTEND":
                return Collections.singletonList(Skills.Category.FRONTEND);
            case "DEV_OPS":
                return Collections.singletonList(Skills.Category.DEV_OPS);

            case "OTHER":
                return Collections.singletonList(Skills.Category.OTHER);

            case "MySkills":
                return Arrays.asList(Skills.Category.BACKEND, Skills.Category.FRONTEND, Skills.Category.OTHER, Skills.Category.DEV_OPS);

            default:
                return Collections.singletonList(Skills.Category.BACKEND);
        }
    }

    public static Skills.Category categoryFormOne(String category) {
        switch (category) {
            case "FRONTEND":
                return Skills.Category.FRONTEND;
            case "DEV_OPS":
                return Skills.Category.DEV_OPS;
            case "OTHER":
                return Skills.Category.OTHER;
            case "MySkills":
                return Skills.Category.MySkills;
            case "TeamSkills":
                return Skills.Category.TeamSkills;
            default:
                return Skills.Category.BACKEND;
        }
    }

    private void validSearchModel(SearchModel searchModel) {
        if(searchModel.getNeedsList() == null || searchModel.getCategory() == null || searchModel.getSizeTeam() == null)
            throw new FieldIsEmptyException("You must choose all options");
        if(searchModel.getSizeTeam() < 2 || searchModel.getSizeTeam() > 10)
            throw new BadRequestException("Team size must be between 2 and 10");
    }
}
