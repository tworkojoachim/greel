package com.Unique.Squad.exceptions;

public class ProfileNotExistsException extends RuntimeException {
    public ProfileNotExistsException(String profile_not_exists) {
        super(profile_not_exists);
    }
}
