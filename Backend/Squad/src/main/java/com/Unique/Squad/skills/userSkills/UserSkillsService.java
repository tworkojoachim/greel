package com.Unique.Squad.skills.userSkills;

import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.skills.userSkills.UserSkills;

import java.util.List;
import java.util.Set;

public interface UserSkillsService {

    UserSkills addUserSkills(UserSkills userSkills, String username);
    List<SkillsDto> getAllUserSkills(String username);
    List<SkillsDto> getAllUserSkillsById(Long id);
    void deleteUserSkill(Long id);
}
