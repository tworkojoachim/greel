package com.Unique.Squad.userProfile;

import com.Unique.Squad.team.TeamInfoDto;
import com.Unique.Squad.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

    UserProfile getUserProfileByUser(User user);

    @Query(value = "SELECT new com.Unique.Squad.userProfile.UserProfileDto(" +
            " t.firstName," +
            " t.lastName," +
            " t.phoneNumber," +
            " t.gitHub," +
            " t.linkedin," +
            " t.position," +
            " t.socialStatus," +
            " t.experience )" +
            " FROM UserProfile t JOIN t.user u " +
            " WHERE u.username = :username ")
    UserProfileDto getUserProfile(String username);

    @Query(value = "SELECT new com.Unique.Squad.userProfile.UserProfileDto(" +
            " t.firstName," +
            " t.lastName," +
            " t.phoneNumber," +
            " t.gitHub," +
            " t.linkedin," +
            " t.position," +
            " t.socialStatus," +
            " t.experience )" +
            " FROM UserProfile t JOIN t.user u " +
            " WHERE u.id = :id ")
    UserProfileDto getUserProfileById(Long id);
}
