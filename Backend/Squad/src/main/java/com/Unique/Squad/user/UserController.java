package com.Unique.Squad.user;

import com.Unique.Squad.jwt.AuthResponse;
import com.Unique.Squad.jwt.JwtResponse;
import com.Unique.Squad.jwt.RefreshToken;
import com.Unique.Squad.registration.RegistrationModel;
import com.Unique.Squad.role.Role;
import com.Unique.Squad.role.RoleRepository;
import com.Unique.Squad.security.AuthService;
import com.Unique.Squad.skills.dateSkills.DateSkills;
import com.Unique.Squad.skills.teamSkills.SearchModel;
import com.Unique.Squad.skills.userSkills.SearchPeopleModel;
import com.Unique.Squad.userProfile.UserProfile;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    private final UserService userService;
    private final AuthService authService;
    private final RefreshToken refreshToken;

    @PostMapping("/registration")
    public void registration(@RequestBody RegistrationModel model){
        userService.registration(model);
    }

    @PostMapping("/authorization")
    public ResponseEntity<AuthResponse> auth(@RequestBody UserDto userDto){
        return ResponseEntity.ok(authService.authenticateUser(userDto));
    }

    @PostMapping("/loggingOut")
    public ResponseEntity<JwtResponse> logout(@RequestBody String token) {
        return ResponseEntity.ok().body(authService.logout(token));
    }

    @PostMapping("/refreshToken")
    public ResponseEntity<JwtResponse> refreshToken(@RequestBody String rToken) {
        return ResponseEntity.ok().body(refreshToken.refreshAccessToken(rToken));
    }

    @PostMapping("/searchPeoples")
    public ResponseEntity<List<UserProfile>> getAllSearchUsersByExpAndSkills(Principal principal, @RequestBody SearchPeopleModel searchModel) {
        return ResponseEntity.ok().body(userService.searchAllUsersProfileBySkillsAndExperience(searchModel, principal.getName()));
    }

}
