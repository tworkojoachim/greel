package com.Unique.Squad.team;

public class TeamConst {

    public final static String TEAM_NAME_VALID = "Team name length min 3 max 30";
    public final static String TEAM_DESCRIPTION_VALID = "Team description must have a minimum of 20 characters";
    public final static String TEAM_PROJECT_VALID = "Team project description must have a minimum of 20 characters";
    public final static String TEAM_PEOPLE_VALID = "Team members description must have a minimum of 20 characters";
    public final static String TEAM_SIZE_VALID = "Team size must be between 2 and 10";
    public final static String TEAM_NULL = "All fields must be completed!";
    public final static String TEAM_NOT_CONSTANT = "One word is a not good description.. :(";
}
