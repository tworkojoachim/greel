package com.Unique.Squad.team;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.NoTeamExceptions;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserInfoDto;
import com.Unique.Squad.user.UserRepository;
import com.Unique.Squad.userProfile.UserProfile;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.Unique.Squad.team.TeamConst.*;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamServiceImpl implements TeamService {

    final TeamRepository teamRepository;
    final UserRepository userRepository;

    @Override
    public Team addTeam(Team team, String username) {
        validTeam(team);
        User user = getUserFromDb(username);
        Team newTeam = Team.builder()
                .name(team.getName())
                .dateOfCreation(format(LocalDate.now()))
                .description(team.getDescription())
                .teamSize(team.getTeamSize())
                .projectDescription(team.getProjectDescription())
                .peopleSought(team.getPeopleSought())
                .userAdmins(Collections.singleton(user))
                .users(Collections.singleton(user))
                .build();

            return teamRepository.save(newTeam);
    }

    @Override
    public LocalDate format(LocalDate localDate) {
        String DATE_FORMATTER = "yyyy-MM-dd";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String data = localDate.format(formatter);
        return LocalDate.parse(data);
    }

    @Override
    public Team getTeamByUser(String username) {
        User user = getUserFromDb(username);
        Optional<Team> team = teamRepository.findByUsers(user);
        if(team.isPresent())
            return team.get();
        else
            throw new NoTeamExceptions("You dont have any team");
    }

    @Override
    public User getUserFromDb(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if(user.isPresent())
            return user.get();
        else
            throw new UsernameNotFoundException("Not found");
    }

    @Override
    public Team getTeam(Long id) {
        Optional<Team> team = teamRepository.findById(id);
        if(team.isPresent())
            return team.get();
        else
            throw new NoTeamExceptions("Team not exists!");
    }

    @Override
    public TeamInfoDto getTeamInfoById(Long id) {
        return teamRepository.getTeamInfo(id);
    }

    @Override
    public boolean existsByUserAdmin(String username) {
        return teamRepository.existsByUserAdmins(getUserFromDb(username));
    }

    @Override
    public boolean existsByUser(String username) {
        return teamRepository.existsByUsers(getUserFromDb(username));
    }

    @Override
    public TeamInfoDto getTeamInfoByUsername(String username) {
        return teamRepository.getTeamInfo(getTeamIdByUser(username));
    }

    @Override
    public Set<UserInfoDto> getAllTeamUsers(String username) {
        Long teamId = getTeamIdByUser(username);
        Long userId = userRepository.findUserIdByUsername(username);
        Set<UserInfoDto> users =  teamRepository.getAllTeamUsers(teamId);
        users.removeIf(userInfoDto -> userInfoDto.getId().equals(userId));
        users.forEach(value -> {
            value.setAdmin(userRepository.isUserAdmin(teamId, value.getId()));
        });

        return users;
    }

    @Override
    public void givePermissions(Long userId, String username) {
        User user = getUserById(userId);
            if(teamRepository.existsByUserAdmins(user))
                throw new BadRequestException("User already have admin permission!");
            else {
                Optional<Team> team = teamRepository.findTeamUsersAdminsByUsername(username);
                team.ifPresent(
                        value -> {
                            value.getUserAdmins().add(user);
                            teamRepository.save(value);
                        });
            }
    }

    @Override
    public void removePermissions(Long userId, String username) {
        User user = getUserById(userId);
            if(!teamRepository.existsByUserAdmins(user))
                throw new BadRequestException("User already don't have admin permission!");
            else {
                Optional<Team> team = teamRepository.findTeamUsersAdminsByUsername(username);
                team.ifPresent(
                        value -> {
                            value.getUserAdmins().remove(user);
                            teamRepository.save(value);
                        });
            }
    }

    @Override
    public void removeUserFromTeam(Long userId, String username) {
        Optional<Team> team = teamRepository.findTeamUsersAdminsByUsername(username);
        if (team.isPresent()) {
            team.get().getUsers().removeIf(user -> user.getId().equals(userId));
            team.get().getUserAdmins().removeIf(admin -> admin.getId().equals(userId));
            teamRepository.save(team.get());
        }
        else throw new BadRequestException("Team not exists");
    }

    public Long getTeamIdByUser(String username) {
        Optional<Long> number = teamRepository.findTeamIdByUserUsername(username);
        if(number.isPresent())
            return number.get();
        else throw new BadRequestException("Team id not exists");
    }

    public User getUserById(Long id) {
        Optional<User> user = userRepository.getUserById(id);
        if(user.isPresent()) return user.get();
        else throw new BadRequestException("User not exists");
    }

    public void validTeam(Team team) {
        if(team.getName() == null || team.getTeamSize() == null || team.getPeopleSought() == null || team.getProjectDescription() == null || team.getDescription() == null)
            throw new BadRequestException(TEAM_NULL);
        else if(team.getName().length() < 3)
            throw new BadRequestException(TEAM_NAME_VALID);
        else if(team.getTeamSize() < 2 || team.getTeamSize() > 10)
            throw new BadRequestException(TEAM_SIZE_VALID);
        else if (team.getDescription().length() < 20)
            throw new BadRequestException(TEAM_DESCRIPTION_VALID);
        else if (!team.getDescription().contains(" "))
            throw new BadRequestException(TEAM_NOT_CONSTANT);
        else if (team.getProjectDescription().length() < 20)
            throw new BadRequestException(TEAM_PROJECT_VALID);
        else if (team.getPeopleSought().length() < 20)
            throw new BadRequestException(TEAM_PEOPLE_VALID);
        else if (!team.getProjectDescription().contains(" "))
            throw new BadRequestException(TEAM_NOT_CONSTANT);
    }

}
