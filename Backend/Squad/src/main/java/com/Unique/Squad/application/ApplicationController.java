package com.Unique.Squad.application;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/application")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ApplicationController {

    final ApplicationService applicationService;

    @PostMapping("/team")
    public void sendApplicationTeam(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody ApplicationRequest applicationDto) {
        applicationService.sendApplicationTeam(applicationDto, user.getPrincipal().toString());
    }

    @PostMapping("/people")
    public void sendApplicationPeople(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody ApplicationRequest applicationDto) {
        applicationService.sendApplicationPeople(applicationDto, user.getPrincipal().toString());
    }

    @GetMapping
    public ResponseEntity<Set<ApplicationResponse>> getAllApplicationByTeam(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(applicationService.getAllByTeamId(user.getPrincipal().toString(), false));
    }

    @GetMapping("/invite")
    public ResponseEntity<Set<ApplicationResponse>> getAllApplicationInvite(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(applicationService.getAllByTeamId(user.getPrincipal().toString(), true));
    }

    @GetMapping("/personA")
    public ResponseEntity<Set<ApplicationResponse>> getAllUserApplications(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(applicationService.getAllUserApplicationOrInvite(user.getPrincipal().toString(), false));
    }

    @GetMapping("/personI")
    public ResponseEntity<Set<ApplicationResponse>> getAllUserInvitations(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(applicationService.getAllUserApplicationOrInvite(user.getPrincipal().toString(), true));
    }

    @PostMapping("/apply")
    public HttpStatus applyApplication(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody ApplicationResponse applicationResponse) {
        applicationService.applyApplication(applicationResponse, user.getPrincipal().toString(), false);
        return HttpStatus.OK;
    }

    @PostMapping("/discard")
    public HttpStatus discardApplication(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody ApplicationResponse applicationResponse) {
        applicationService.discardApplication(applicationResponse, user.getPrincipal().toString(), false);
        return HttpStatus.OK;
    }

    @PostMapping("/applyUser")
    public HttpStatus applyApplicationUser(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody ApplicationResponse applicationResponse) {
        applicationService.applyApplication(applicationResponse, user.getPrincipal().toString(), true);
        return HttpStatus.OK;
    }

    @PostMapping("/discardUser")
    public HttpStatus discardApplicationUser(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody ApplicationResponse applicationResponse) {
        applicationService.discardApplication(applicationResponse, user.getPrincipal().toString(), true);
        return HttpStatus.OK;
    }
}
