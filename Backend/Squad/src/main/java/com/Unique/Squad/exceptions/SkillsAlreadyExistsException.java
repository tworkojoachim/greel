package com.Unique.Squad.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SkillsAlreadyExistsException extends RuntimeException {
    public SkillsAlreadyExistsException(String you_already_have_this_skill) {
        super(you_already_have_this_skill);
    }
}
