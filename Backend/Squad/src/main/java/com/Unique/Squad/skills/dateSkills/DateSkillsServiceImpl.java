package com.Unique.Squad.skills.dateSkills;

import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.teamSkills.TeamSkillsRepository;
import com.Unique.Squad.skills.teamSkills.TeamSkillsServiceImpl;
import com.Unique.Squad.skills.userSkills.UserSkillsRepository;
import com.Unique.Squad.team.TeamRepository;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DateSkillsServiceImpl implements DateSkillsService {

    final DateSkillsRepository dateSkillsRepository;
    final UserRepository userRepository;
    final UserSkillsRepository userSkillsRepository;
    final TeamRepository teamRepository;
    final TeamSkillsRepository teamSkillsRepository;

    @Override
    public List<DateSkills> getAllByCategory(String category, String username) {
        Skills.Category newCategory = TeamSkillsServiceImpl.categoryFormOne(category);

        switch (newCategory) {

            case MySkills:
                Optional<User> user = userRepository.findByUsername(username);
                if(user.isPresent())
                    return userSkillsRepository.findAllByUserSearch(user.get());

            case TeamSkills:
                Optional<Long> teamId = teamRepository.findTeamIdByUserUsername(username);
                if(teamId.isPresent())
                    return teamSkillsRepository.findAllRequiredTeamSkills(teamId.get());

            default:
                return dateSkillsRepository.findByCategory(newCategory);
        }
    }

    @Override
    public DateSkills getByName(String name) {
        return dateSkillsRepository.findByName(name);
    }
}
