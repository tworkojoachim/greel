package com.Unique.Squad.jwt;

import com.Unique.Squad.user.LoadUserService;
import com.Unique.Squad.user.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

import static com.Unique.Squad.constant.JWTConstant.*;

@Component
@Log4j2
public class JwtRequestFilter extends OncePerRequestFilter {

    private final JwtUtils jwtUtils;
    private final LoadUserService loadUserService;
    private final UserRepository userRepository;

    public JwtRequestFilter(JwtUtils jwtUtils, LoadUserService loadUserService, UserRepository userRepository) {
        this.jwtUtils = jwtUtils;
        this.loadUserService = loadUserService;
        this.userRepository = userRepository;
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String requestedEndPoint = request.getServletPath();
        logger.info("Request to -> " + requestedEndPoint + ", method: " + request.getMethod());
        return Arrays.stream(new String[]{"/authorization", "/registration", "/role", "/user", "/loggingOut", "/refreshToken"})
                .anyMatch(requestedEndPoint::startsWith);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

            UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
            if(authentication == null) {
                filterChain.doFilter(request, response);
                return;
            }

            SecurityContextHolder.getContext().setAuthentication(authentication);
            filterChain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(TOKEN_HEADER);
        if(userRepository.existsByToken(token)) {
            if (token != null) {
                String userName = jwtUtils.getUsernameFromJWT(token);
                if (userName != null) {
                    UserDetails userDetails = loadUserService.loadUserByUsername(userName);
                    return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), null, userDetails.getAuthorities());
                }
            }
        }
        return null;
    }

}
