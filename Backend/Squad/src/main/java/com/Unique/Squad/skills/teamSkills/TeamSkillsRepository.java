package com.Unique.Squad.skills.teamSkills;

import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.skills.dateSkills.DateSkills;
import com.Unique.Squad.team.Team;
import com.Unique.Squad.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Repository
@Transactional
public interface TeamSkillsRepository extends JpaRepository<TeamSkills, Long> {

    @Query(value = "SELECT " +
            " new com.Unique.Squad.skills.dateSkills.DateSkills( " +
            " s.id, " +
            " s.category, " +
            " s.name) " +
            " FROM TeamSkills s JOIN s.team t" +
            " WHERE t.id = :id")
    List<DateSkills> findAllRequiredTeamSkills(Long id);

    @Query(value = "SELECT " +
            " new com.Unique.Squad.skills.SkillsDto( " +
            " s.id, " +
            " s.name, " +
            " s.level," +
            " s.category )" +
            " FROM TeamSkills s" +
            " WHERE s.team = :team")
    List<SkillsDto> findAllByTeam(Team team);

    @Query(value = "SELECT " +
            " new com.Unique.Squad.skills.SkillsDto( " +
            " s.id, " +
            " s.name, " +
            " s.level," +
            " s.category )" +
            " FROM TeamSkills s JOIN s.team t " +
            " WHERE t.id = :id")
    Set<SkillsDto> findAllByTeamId(Long id);

    @Query(value = "SELECT " +
            " t.name " +
            " FROM TeamSkills t " +
            " WHERE t.team = :team")
    List<String> getNeedsSkillName(Team team);
}
