package com.Unique.Squad.userProfile;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.ProfileNotExistsException;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.Unique.Squad.userProfile.UserProfileConstant.FIRST_NAME_VALID;
import static com.Unique.Squad.userProfile.UserProfileConstant.LAST_NAME_VALID;

@Service
@Log4j2
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserProfileServiceImpl implements UserProfileService {

    final UserRepository userRepository;
    final UserProfileRepository userProfileRepository;
    final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    public UserProfileServiceImpl(UserRepository userRepository, UserProfileRepository userProfileRepository) {
        this.userRepository = userRepository;
        this.userProfileRepository = userProfileRepository;
    }

    @Override
    public UserProfileDto getUserProfile(String username) {
            return userProfileRepository.getUserProfile(username);
    }

    @Override
    public UserProfileDto getUserProfileById(Long id) {
            return userProfileRepository.getUserProfileById(id);
    }

    @Override
    public UserProfile save(UserProfile userProfile, String username) {
        log.info(userProfile.toString());
        validUserProfile(userProfile);
        Optional<User> user = userRepository.findByUsername(username);
        if(user.isPresent()){
             UserProfile profile = userProfileRepository.getUserProfileByUser(user.get());
                profile.setFirstName(userProfile.getFirstName());
                profile.setLastName(userProfile.getLastName());
             if(userProfile.getPhoneNumber() != null)
                profile.setPhoneNumber(userProfile.getPhoneNumber());
                profile.setExperience(userProfile.getExperience());
             if(userProfile.getSocialStatus() != null)
                profile.setSocialStatus(userProfile.getSocialStatus());
             if(userProfile.getPosition() != null)
                profile.setPosition(userProfile.getPosition());
             if(userProfile.getGitHub() != null)
                profile.setGitHub(userProfile.getGitHub());
             if(userProfile.getLinkedin() != null)
                profile.setLinkedin(userProfile.getLinkedin());
             return userProfileRepository.save(profile);
        }
        else throw new ProfileNotExistsException("Profile not exists");
    }

    public void validUserProfile(UserProfile userProfile) {
        log.info(String.valueOf(userProfile.getFirstName().length()));
        if(userProfile.getFirstName().length() < 3 || userProfile.getFirstName().length() > 25)
            throw new BadRequestException(FIRST_NAME_VALID);
        else if(userProfile.getLastName().length() < 3 || userProfile.getLastName().length() > 25)
            throw new BadRequestException(LAST_NAME_VALID);
    }
}
