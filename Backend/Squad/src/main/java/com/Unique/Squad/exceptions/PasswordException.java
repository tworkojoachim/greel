package com.Unique.Squad.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class PasswordException extends RuntimeException{

    public PasswordException(String message) {
        super(message);
    }
}
