package com.Unique.Squad.user;

import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.teamSkills.TeamSearchModel;
import com.Unique.Squad.userProfile.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.hibernate.loader.Loader.SELECT;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    Optional<User> findByToken(String token);
    Optional<User> findByRefreshToken(String rToken);
    boolean existsByToken(String token);

    @Query(value = "SELECT DISTINCT new com.Unique.Squad.userProfile.UserProfile(" +
            " p.id, " +
            " p.firstName, " +
            " p.gitHub, " +
            " p.linkedin, " +
            " p.experience, " +
            " p.position, " +
            " p.socialStatus) " +
            " FROM User u JOIN u.userProfile p JOIN u.skills s " +
            " WHERE p.experience >= :experience " +
            " AND s.category IN :category " +
            " And s.name IN :names " +
            " AND u.username <> :username")
    List<UserProfile> getAllSearchUsersProfile(Long experience, @Param("names") Collection<String> names, Collection<Skills.Category> category, String username);

    @Query(value = "SELECT " +
            "u.id " +
            "FROM User u " +
            "WHERE u.username = :username ")
    Long findUserIdByUsername(String username);

    @Query(value = "SELECT CASE WHEN EXISTS( " +
            " SELECT team_id, admin_id " +
            " FROM team_admin t " +
            " WHERE team_id = :teamId " +
            " AND admin_id = :adminId ) " +
            " THEN CAST (1 AS BIT) " +
            " ELSE CAST (0 AS BIT) END", nativeQuery = true)
    boolean isUserAdmin(Long teamId, Long adminId);

    @Modifying
    @Query(value = "INSERT INTO team_user VALUES (?1, ?2);", nativeQuery = true)
    void addUserToTeam( Long teamId, Long userId);

    @Query(value = "SELECT " +
            " u " +
            " FROM User u " +
            " WHERE u.username = :username")
    Optional<User> getUserByUsername(String username);

    @Query(value = "SELECT " +
            " u " +
            " FROM User u " +
            " WHERE u.id = :id")
    Optional<User> getUserById(Long id);

}
