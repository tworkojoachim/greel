package com.Unique.Squad.skills;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SkillsDto {

    Long id;
    String name;
    Long numberOfMonth;
    Skills.Level level;
    Skills.Category category;

    public SkillsDto(Long id, String name, Skills.Level level, Skills.Category category) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.category = category;
    }
}
