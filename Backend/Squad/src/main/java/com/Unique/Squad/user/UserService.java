package com.Unique.Squad.user;

import com.Unique.Squad.jwt.JwtResponse;
import com.Unique.Squad.registration.RegistrationModel;
import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.teamSkills.SearchModel;
import com.Unique.Squad.skills.userSkills.SearchPeopleModel;
import com.Unique.Squad.userProfile.UserProfile;

import java.util.List;
import java.util.Set;

public interface UserService {

    void registration(RegistrationModel registrationModel);
    void userValidationByUsername(String username);
    void userValidationByEmail(String email);
    void validation(RegistrationModel model);
    List<UserProfile> searchAllUsersProfileBySkillsAndExperience(SearchPeopleModel searchModel, String username);
}
