package com.Unique.Squad.team;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamInfoDto {

    String name;
    String description;
    String projectDescription;
    String peopleSought;
    Long teamSize;
    Long membersValue;
    LocalDate dateOfCreation;

    public TeamInfoDto(String name, String description, String projectDescription, String peopleSought, Long teamSize, Long membersValue, LocalDate dateOfCreation) {
        this.name = name;
        this.description = description;
        this.projectDescription = projectDescription;
        this.peopleSought = peopleSought;
        this.teamSize = teamSize;
        this.membersValue = membersValue;
        this.dateOfCreation = dateOfCreation;
    }

}
