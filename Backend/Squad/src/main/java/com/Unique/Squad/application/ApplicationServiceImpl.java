package com.Unique.Squad.application;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.NoTeamExceptions;
import com.Unique.Squad.team.Team;
import com.Unique.Squad.team.TeamRepository;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ApplicationServiceImpl implements ApplicationService{

    final ApplicationRepository applicationRepository;
    final UserRepository userRepository;
    final TeamRepository teamRepository;

    @Override
    public void sendApplicationTeam(ApplicationRequest applicationDto, String username) {
        Optional<User> user = userRepository.findByUsername(username);
        valid(username, applicationDto.getTeamId(), user.get().getId());
        Application application = new Application();
        application.setUserInvite(false);
        Optional<Team> team = teamRepository.findById(applicationDto.getTeamId());
        user.ifPresent(application::setUser);
        team.ifPresent(application::setTeam);
        application.setDate(LocalDateTime.now());
        application.setMessage(applicationDto.getMessage());
        applicationRepository.save(application);
    }

    @Override
    public void sendApplicationPeople(ApplicationRequest applicationDto, String username) {
        Application application = new Application();
        application.setUserInvite(true);
        Optional<User> user = userRepository.findById(applicationDto.getTeamId());
        Optional<Team> team = teamRepository.findTeamByUserUsername(username);
        valid(user.get().getUsername(), team.get().getId(), applicationDto.getTeamId());
        team.ifPresent(application::setTeam);
        user.ifPresent(application::setUser);
        application.setDate(LocalDateTime.now());
        application.setMessage(applicationDto.getMessage());
        applicationRepository.save(application);
    }

    @Override
    public Set<ApplicationResponse> getAllByTeamId(String username, boolean value) {
        Optional<Long> number = teamRepository.findTeamIdByUserUsername(username);
        if(number.isPresent()) {
            return applicationRepository.getAllByTeamId(number.get(), value);
        }
        throw new NoTeamExceptions("Id not found!");
    }

    @Override
    public Set<ApplicationResponse> getAllUserApplicationOrInvite(String username, boolean value) {
        Optional<User> user = userRepository.findByUsername(username);
        if(user.isPresent())
            return applicationRepository.getAllUserApplicationOrInvite(username, value);
        else throw new UsernameNotFoundException("User not found!");
    }

    @Override
    public void applyApplication(ApplicationResponse applicationResponse, String username, boolean value) {
       if(isAdmin(applicationResponse.getTeam(), username) || value) {
           if(!teamRepository.existsByUsers(userRepository.findById(applicationResponse.getUser()).get())) {
               userRepository.addUserToTeam(applicationResponse.getTeam(), applicationResponse.getUser());
               applicationRepository.deleteById(applicationResponse.getId());
           }
           else if(value) throw new BadRequestException("You have to leave the team to join the new one!");
           else throw new BadRequestException("The user is currently on the team!");
       }
       else throw new BadRequestException("You don't have team manager privileges");
    }

    @Override
    public void discardApplication(ApplicationResponse applicationResponse, String username, boolean value) {
        if(isAdmin(applicationResponse.getTeam(), username)  || value) {
           applicationRepository.deleteById(applicationResponse.getId());
        }
        else throw new BadRequestException("You don't have team manager privileges");
    }

    public void valid(String username, Long teamId, Long userId) {
        Optional<Long> check = teamRepository.findTeamIdByUserUsername(username);
        if(check.isPresent() && check.get().equals(teamId))
            throw new BadRequestException("You cannot apply to the team you are in!");
        if(applicationRepository.existsByTeamIdAndUserId(teamId, userId))
            throw new BadRequestException("You have an invitation or have already sent an application to this team!");
    }

    private boolean isAdmin(Long teamId, String username){
        Long value = userRepository.findUserIdByUsername(username);
        return userRepository.isUserAdmin(teamId, value);
    }
}
