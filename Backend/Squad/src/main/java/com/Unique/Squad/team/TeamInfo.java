package com.Unique.Squad.team;

public interface TeamInfo {
    String getName();
    String getDescription();
    String getProjectDescription();
    String getPeopleSought();
    Long getTeamSize();
    Integer getMembersValue();
    String getDateOfCreation();
}
