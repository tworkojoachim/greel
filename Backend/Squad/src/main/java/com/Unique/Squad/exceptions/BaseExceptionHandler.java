package com.Unique.Squad.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger("BaseExceptionHandler");

    public abstract ResponseEntity<ErrorMessage> handle(Exception e, HttpServletRequest request);

    public ErrorMessage createErrorMessage(String message, HttpServletRequest request, HttpStatus httpStatus){
        return new ErrorMessage(httpStatus.value(), httpStatus.toString(),
                message, request.getServletPath());
    }
}
