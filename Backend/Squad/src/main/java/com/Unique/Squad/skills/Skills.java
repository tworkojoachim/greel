package com.Unique.Squad.skills;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.logging.Level;

@Data
@MappedSuperclass
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class Skills {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    Long id;

    String name;
    Long numberOfMonth;
    Level level;
    Category category;

    public static enum Level {DEFAULT, BEGINNER, INTERMEDIATE, ADVANCED, EXPERT, MASTER;}
    public static enum Category { BACKEND, FRONTEND, DEV_OPS, OTHER, MySkills, TeamSkills;}

}
