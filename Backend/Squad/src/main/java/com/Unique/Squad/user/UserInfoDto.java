package com.Unique.Squad.user;

import com.Unique.Squad.userProfile.UserProfile;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserInfoDto {

    Long id;
    String firstName;
    String lastName;
    String phoneNumber;
    String gitHub;
    String linkedin;
    boolean isAdmin;

    public UserInfoDto(UserProfile userProfile) {
        this.firstName = userProfile.getFirstName();
        this.lastName = userProfile.getLastName();
        this.phoneNumber = userProfile.getPhoneNumber();
        this.gitHub = userProfile.getGitHub();
        this.linkedin = userProfile.getLinkedin();
    }

    public UserInfoDto(String firstName, String lastName, String phoneNumber, String gitHub, String linkedin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.gitHub = gitHub;
        this.linkedin = linkedin;
    }

    public UserInfoDto(Long id, String firstName, String lastName, String phoneNumber, String gitHub, String linkedin) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.gitHub = gitHub;
        this.linkedin = linkedin;
    }
}
