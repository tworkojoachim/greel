package com.Unique.Squad.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class AccountNotConfirmedException extends RuntimeException {
    public AccountNotConfirmedException(String account_not_confirmed) {
        super(account_not_confirmed);
    }
}
