package com.Unique.Squad.team;

import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserInfoDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/team")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TeamController {

    final TeamService teamService;
    final TeamServiceImpl teamServiceImpl;

    @GetMapping("/id")
    public ResponseEntity<Long> getTeamIdByUser(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(teamServiceImpl.getTeamIdByUser(user.getPrincipal().toString()));
    }

    @GetMapping
    public ResponseEntity<TeamInfoDto> getTeam(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(teamService.getTeamInfoByUsername(user.getPrincipal().toString()));
    }

    @GetMapping("{id}")
    public ResponseEntity<TeamInfoDto> getTeamById(@PathVariable Long id) {
        return ResponseEntity.ok().body(teamService.getTeamInfoById(id));
    }

    @GetMapping("/users")
    public ResponseEntity<Set<UserInfoDto>> getTeamUsers(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(teamService.getAllTeamUsers(user.getPrincipal().toString()));
    }

    @GetMapping("/existBy")
    public ResponseEntity<Boolean> userHaveTeam(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        if(user != null)
            return ResponseEntity.ok().body(teamService.existsByUser(user.getPrincipal().toString()));
        else
            return ResponseEntity.ok().body(false);
    }

    @GetMapping("/admin")
    public ResponseEntity<Boolean> isAdmin(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        if(user != null)
            return ResponseEntity.ok().body(teamService.existsByUserAdmin(user.getPrincipal().toString()));
        else
            return ResponseEntity.ok().body(false);
    }

    @PostMapping
    public ResponseEntity<Team> addTeam(@RequestBody Team team, @AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(teamService.addTeam(team, user.getPrincipal().toString()));
    }

    @DeleteMapping(value = "/remove/{id}")
    public HttpStatus removeUser(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @PathVariable("id") Long id) {
        teamService.removeUserFromTeam(id, user.getPrincipal().toString());
        return HttpStatus.OK;
    }

    @PostMapping("/permission/add/{id}")
    public HttpStatus addPermission(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @PathVariable("id") Long id) {
        teamService.givePermissions(id, user.getPrincipal().toString());
        return HttpStatus.OK;
    }

    @PostMapping("/permission/remove/{id}")
    public HttpStatus removePermission(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @PathVariable("id") Long id) {
        teamService.removePermissions(id, user.getPrincipal().toString());
        return HttpStatus.OK;
    }
}
