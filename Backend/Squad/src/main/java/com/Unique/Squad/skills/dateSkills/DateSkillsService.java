package com.Unique.Squad.skills.dateSkills;

import com.Unique.Squad.skills.Skills;

import java.util.List;

public interface DateSkillsService {

    List<DateSkills> getAllByCategory(String category, String username);
    DateSkills getByName(String name);
}
