package com.Unique.Squad.validation;

import com.Unique.Squad.exceptions.PasswordNotMatchException;
import org.passay.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class PasswordValidation {

    public boolean isValid(String password, String confirmPassword){

        if(password.equals(confirmPassword)){
            final PasswordValidator validator = new PasswordValidator(Arrays.asList(
                    new LengthRule(8, 30),
                    new UppercaseCharacterRule(1),
                    new DigitCharacterRule(1),
                    new SpecialCharacterRule(1),
                    new WhitespaceRule()));

            final RuleResult result = validator.validate(new PasswordData(password));
            return result.isValid();
        }
        else throw new PasswordNotMatchException("Password not match");
    }
}

