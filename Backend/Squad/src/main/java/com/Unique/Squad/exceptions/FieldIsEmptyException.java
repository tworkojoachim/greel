package com.Unique.Squad.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class FieldIsEmptyException extends RuntimeException {

    public FieldIsEmptyException(String message){
        super(message);
    }
}
