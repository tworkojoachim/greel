package com.Unique.Squad.skills.teamSkills;

import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.team.Team;
import com.Unique.Squad.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
@Entity
@Builder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamSkills extends Skills {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    Team team;

    public TeamSkills(Team team) {
        super();
        this.team = team;
    }

}
