package com.Unique.Squad.skills.userSkills;

import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.skills.userSkills.UserSkills;
import com.Unique.Squad.skills.userSkills.UserSkillsService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/skills")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserSkillsController {

    final UserSkillsService userSkillsService;

    @GetMapping("/user")
    public ResponseEntity<List<SkillsDto>> getAllUserSkills(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(userSkillsService.getAllUserSkills(user.getPrincipal().toString()));
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<SkillsDto>> getAllUserSkillsById(@PathVariable Long userId) {
        return ResponseEntity.ok().body(userSkillsService.getAllUserSkillsById(userId));
    }

    @PostMapping("/user")
    public ResponseEntity<UserSkills> addSkill(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody UserSkills userSkills) {
        return ResponseEntity.ok().body(userSkillsService.addUserSkills(userSkills, user.getPrincipal().toString()));
    }

    @DeleteMapping("/user/{id}")
    public HttpStatus deleteSkill(@PathVariable("id") Long id) {
        userSkillsService.deleteUserSkill(id);
        return HttpStatus.OK;
    }

}
