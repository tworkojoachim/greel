package com.Unique.Squad.skills.dateSkills;

import com.Unique.Squad.skills.Skills;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DateSkills {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "CATEGORY", length = 60)
    Skills.Category category;

    @UniqueElements
    String name;

}
