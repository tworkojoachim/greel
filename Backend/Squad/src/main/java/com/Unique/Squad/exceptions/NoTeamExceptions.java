package com.Unique.Squad.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.OK)
public class NoTeamExceptions extends RuntimeException {
    public NoTeamExceptions(String you_dont_have_any_team) {
        super(you_dont_have_any_team);
    }
}
