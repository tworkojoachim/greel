package com.Unique.Squad.skills.teamSkills;

import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.team.Team;

import java.util.List;
import java.util.Set;

public interface TeamSkillsService {

    TeamSkills addSkill(TeamSkills teamSkills, String username);
    List<SkillsDto> getAllByTeam(String username);
    Set<SkillsDto> getAllByTeamId(Long id);
    void deleteSkill(Long id);
    Team findTeamByUsername(String username);
    List<TeamSearchModel> getAllSearchTeams(SearchModel searchModel);
}
