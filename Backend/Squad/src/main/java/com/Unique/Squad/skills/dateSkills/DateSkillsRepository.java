package com.Unique.Squad.skills.dateSkills;

import com.Unique.Squad.skills.Skills;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface DateSkillsRepository extends JpaRepository<DateSkills, Long> {

    List<DateSkills> findByCategory(Skills.Category category);
    DateSkills findByName(String name);
}
