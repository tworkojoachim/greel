package com.Unique.Squad.registration;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Builder
public class RegistrationModel {

    String username;
    String password;
    String confirmPassword;
    String email;
}
