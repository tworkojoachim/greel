package com.Unique.Squad.userProfile;

import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.Authenticator;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/profile")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserProfileController {

    final UserProfileService userProfileService;

    @GetMapping
    public ResponseEntity<UserProfileDto> getUserProfile(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user) {
        return ResponseEntity.ok().body(userProfileService.getUserProfile(user.getPrincipal().toString()));
    }

    @GetMapping("{id}")
    public ResponseEntity<UserProfileDto> getUserProfileById(@PathVariable Long id) {
        return ResponseEntity.ok().body(userProfileService.getUserProfileById(id));
    }

    @PutMapping
    public ResponseEntity<UserProfile> saveProfile(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @RequestBody UserProfile userProfile) {
        return ResponseEntity.ok().body(userProfileService.save(userProfile, user.getPrincipal().toString()));
    }

}
