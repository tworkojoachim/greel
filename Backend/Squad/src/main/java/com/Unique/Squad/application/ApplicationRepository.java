package com.Unique.Squad.application;

import com.Unique.Squad.team.Team;
import com.Unique.Squad.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    @Query(value = "SELECT CASE WHEN EXISTS( " +
            " SELECT team_id " +
            " FROM application " +
            " WHERE team_id = :teamId " +
            " AND user_id = :userId ) " +
            " THEN CAST (1 AS BIT) " +
            " ELSE CAST (0 AS BIT) END", nativeQuery = true)
    boolean existsByTeamIdAndUserId(Long teamId, Long userId);

    @Query(value = "SELECT new com.Unique.Squad.application.ApplicationResponse( " +
            " a.id, " +
            " a.message, " +
            " a.date, " +
            " u.id, " +
            " t.id )" +
            " FROM Application a JOIN a.team t JOIN a.user u " +
            " WHERE t.id = :id" +
            " AND a.userInvite = :value")
    Set<ApplicationResponse> getAllByTeamId(Long id, boolean value);

    @Query(value = "SELECT new com.Unique.Squad.application.ApplicationResponse( " +
            " a.id, " +
            " a.message, " +
            " a.date, " +
            " u.id, " +
            " t.id )" +
            " FROM Application a JOIN a.team t JOIN a.user u " +
            " WHERE u.username = :username " +
            " AND a.userInvite = :value ")
    Set<ApplicationResponse> getAllUserApplicationOrInvite(String username, boolean value);


}
