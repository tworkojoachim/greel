package com.Unique.Squad.skills.userSkills;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SearchPeopleModel {

        Long experience;
        List<String> needsList;
        String category;
}
