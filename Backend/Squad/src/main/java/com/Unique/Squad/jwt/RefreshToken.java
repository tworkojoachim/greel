package com.Unique.Squad.jwt;

import com.Unique.Squad.exceptions.SessionExpiredException;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserDto;
import com.Unique.Squad.user.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RefreshToken {

    private final UserRepository userRepository;
    private final JwtUtils jwtUtils;

    public RefreshToken(UserRepository userRepository, JwtUtils jwtUtils) {
        this.userRepository = userRepository;
        this.jwtUtils = jwtUtils;
    }

    public String createRefreshToken(User user) {
        String rToken = RandomStringUtils.randomAlphanumeric(128);
        user.setRefreshToken(rToken);
        userRepository.save(user);
        return rToken;
    }

    public JwtResponse refreshAccessToken(String rToken) {
        Optional<User> user = userRepository.findByRefreshToken(rToken);
        if(user.isPresent()) {
            UserDto userDto = UserDto.create(user.get());
            String jwt = jwtUtils.generateToken(userDto);
            user.get().setToken(jwt);
            userRepository.save(user.get());
            return new JwtResponse(jwt);
        }
        else throw new SessionExpiredException("Please log in");
    }

}
