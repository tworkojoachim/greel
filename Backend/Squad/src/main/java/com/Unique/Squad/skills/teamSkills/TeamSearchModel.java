package com.Unique.Squad.skills.teamSkills;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamSearchModel {

    Long id;
    String name;
    String description;
    String peopleSought;
    Long teamSize;
}
