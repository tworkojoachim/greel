package com.Unique.Squad.skills.dateSkills;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/data")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DateSkillsController {

    final DateSkillsService dateSkillsService;

    @GetMapping("/category/{category}")
    public ResponseEntity<List<DateSkills>> getAllSkillsByCategory(@AuthenticationPrincipal UsernamePasswordAuthenticationToken user, @PathVariable String category) {
        return ResponseEntity.ok().body(dateSkillsService.getAllByCategory(category, user.getPrincipal().toString()));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<DateSkills> getSkillsByName(@PathVariable String name) {
        return ResponseEntity.ok().body(dateSkillsService.getByName(name));
    }
}
