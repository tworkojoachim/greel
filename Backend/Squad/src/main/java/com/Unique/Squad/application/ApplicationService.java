package com.Unique.Squad.application;

import java.util.Set;

public interface ApplicationService {

    void sendApplicationTeam(ApplicationRequest applicationDto, String username);
    void sendApplicationPeople(ApplicationRequest applicationDto, String username);
    Set<ApplicationResponse> getAllByTeamId(String username, boolean value);
    Set<ApplicationResponse> getAllUserApplicationOrInvite(String username, boolean value);
    void applyApplication(ApplicationResponse applicationResponse, String username, boolean value);
    void discardApplication(ApplicationResponse applicationResponse, String username, boolean value);

}
