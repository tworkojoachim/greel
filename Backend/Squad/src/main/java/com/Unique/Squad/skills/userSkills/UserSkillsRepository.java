package com.Unique.Squad.skills.userSkills;

import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.skills.dateSkills.DateSkills;
import com.Unique.Squad.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
@Transactional
public interface UserSkillsRepository extends JpaRepository<UserSkills, Long> {

    Optional<UserSkills> findByName(String name);

    @Query(value = "SELECT " +
            " new com.Unique.Squad.skills.dateSkills.DateSkills( " +
            " s.id, " +
            " s.category, " +
            " s.name) " +
            " FROM UserSkills s" +
            " WHERE s.user = :user")
    List<DateSkills> findAllByUserSearch(User user);

    @Query(value = "SELECT " +
            " new com.Unique.Squad.skills.SkillsDto( " +
            " s.id, " +
            " s.name, " +
            " s.numberOfMonth," +
            " s.level," +
            " s.category )" +
            " FROM UserSkills s" +
            " WHERE s.user = :user")
    List<SkillsDto> findAllByUser(User user);

}
