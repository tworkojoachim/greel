package com.Unique.Squad.team;

import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.teamSkills.TeamSearchModel;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    Optional<Team> findByUsers( User userAdmin);

    @Query(value = "SELECT " +
            "t.id " +
            "FROM Team t JOIN t.users u " +
            "WHERE u.username = :username ")
    Optional<Long> findTeamIdByUserUsername(String username);

    @Query(value = "SELECT " +
            " t " +
            "FROM Team t JOIN t.users u JOIN t.userAdmins a " +
            "WHERE u.username = :username ")
    Optional<Team> findTeamUsersAdminsByUsername(String username);

    @Query(value = "SELECT DISTINCT new com.Unique.Squad.skills.teamSkills.TeamSearchModel(" +
            " t.id, " +
            " t.name, " +
            " t.description, " +
            " t.peopleSought, " +
            " t.teamSize ) " +
            " FROM Team t JOIN t.teamSkills s " +
            " WHERE t.teamSize <= :size " +
            " AND s.category IN :category" +
            " And s.name IN :names")
    List<TeamSearchModel> getAllSearchTeams(Long size, @Param("names") Collection<String> names, Collection<Skills.Category> category);

    @Query(value = "SELECT t " +
            "FROM Team t JOIN t.users u " +
            "WHERE u.username = :username")
    Optional<Team> findTeamByUserUsername(String username);

    @Query(value = "SELECT t FROM Team t WHERE t.id = :id")
    Optional<Team> findById(Long id);

    boolean existsByUsers(User user);

    boolean existsByUserAdmins(User user);

    @Query(value = "SELECT new com.Unique.Squad.team.TeamInfoDto(" +
            "t.name," +
            " t.description," +
            " t.projectDescription," +
            " t.peopleSought," +
            " t.teamSize," +
            " COUNT(u), " +
            " t.dateOfCreation )" +
            " FROM Team t JOIN t.users u" +
            " WHERE t.id = :id" +
            " GROUP BY t.name, t.description, t.projectDescription, t.peopleSought, t.teamSize, t.dateOfCreation")
    TeamInfoDto getTeamInfo(Long id);

    @Query(value = "SELECT new com.Unique.Squad.user.UserInfoDto(" +
            " p.id, " +
            " p.firstName, " +
            " p.lastName, " +
            " p.phoneNumber, " +
            " p.gitHub, " +
            " p.linkedin) " +
            " FROM Team t JOIN t.users u JOIN u.userProfile p " +
            " WHERE t.id = :teamId ")
    Set<UserInfoDto> getAllTeamUsers(Long teamId);

}
