DELETE FROM ROLE;
INSERT INTO ROLE VALUES (1, 'USER');
INSERT INTO ROLE VALUES (2, 'ADMIN');
INSERT INTO ROLE VALUES (3, 'MANAGER');

DELETE FROM USER_PROFILE;
INSERT INTO USER_PROFILE VALUES (1, 24, 'firstName', 'github', 'lastName', 'linkedin', '324234324', 'FULL_STACK', 'STUDENT');
INSERT INTO USER_PROFILE VALUES (2, 24, 'firstName', 'github', 'lastName', 'linkedin', '324234324', 'FULL_STACK', 'STUDENT');
INSERT INTO USER_PROFILE VALUES (3, 24, 'firstName', 'github', 'lastName', 'linkedin', '324234324', 'FULL_STACK', 'STUDENT');

DELETE FROM APP_USER CASCADE;
INSERT INTO APP_USER VALUES (1, 'email@email.com','', '$2a$10$T0HVBcJGi9MuOrf0CSlA.OhijpFtZAh7bT1Qszuy2uSZ/JroGSWR6', 'ACTIVE', 'username', 1);
INSERT INTO APP_USER VALUES (2, 'email@xd.com','', '$2a$10$T0HVBcJGi9MuOrf0CSlA.OhijpFtZAh7bT1Qszuy2uSZ/JroGSWR6', 'ACTIVE', 'username2', 2);
INSERT INTO APP_USER VALUES (3, 'email@gmail.com','', '$2a$10$T0HVBcJGi9MuOrf0CSlA.OhijpFtZAh7bT1Qszuy2uSZ/JroGSWR6', 'ACTIVE', 'username3', 3);

DELETE FROM TEAM;
INSERT INTO TEAM VALUES (1, '2020-08-23', 'its amazing description omg', 'amazingName', 'Amazing people!', 'Project amzing!', 5);

DELETE FROM TEAM_USER;
INSERT INTO TEAM_USER VALUES (1, 2);
INSERT INTO TEAM_USER VALUES (1, 3);

DELETE FROM TEAM_ADMIN;
INSERT INTO TEAM_ADMIN VALUES (1, 2);

DELETE FROM TEAM_SKILLS;
INSERT INTO TEAM_SKILLS VALUES (1, 0, 2, 'C++', 24, 1);
INSERT INTO TEAM_SKILLS VALUES (2, 0, 2, 'C', 12, 1);

DELETE FROM USER_ROLE;
INSERT INTO USER_ROLE VALUES (1, 1);

DELETE FROM USER_SKILLS;
INSERT INTO USER_SKILLS VALUES (1, 0, 3, 'Java', 3, 1);

DELETE FROM date_skills;
INSERT INTO date_skills VALUES (1, 'BACKEND', 'Java');
INSERT INTO date_skills VALUES (2, 'BACKEND', 'Kotlin');
INSERT INTO date_skills VALUES (3, 'BACKEND', 'C');
INSERT INTO date_skills VALUES (4, 'BACKEND', 'C++');
INSERT INTO date_skills VALUES (5, 'BACKEND', 'C#');
INSERT INTO date_skills VALUES (6, 'BACKEND', 'PHP');
INSERT INTO date_skills VALUES (7, 'BACKEND', 'Ruby');
INSERT INTO date_skills VALUES (8, 'BACKEND', 'Python');
INSERT INTO date_skills VALUES (9, 'BACKEND', 'Perl');
INSERT INTO date_skills VALUES (10, 'BACKEND', 'Prolog');

INSERT INTO date_skills VALUES (11, 'FRONTEND', 'JavaScript');
INSERT INTO date_skills VALUES (12, 'FRONTEND', 'HTML');
INSERT INTO date_skills VALUES (13, 'FRONTEND', 'CSS');
INSERT INTO date_skills VALUES (14, 'FRONTEND', 'TypeScript');
INSERT INTO date_skills VALUES (15, 'FRONTEND', 'Angular');
INSERT INTO date_skills VALUES (16, 'FRONTEND', 'React');
INSERT INTO date_skills VALUES (17, 'FRONTEND', 'Vue Js');
INSERT INTO date_skills VALUES (18, 'FRONTEND', 'Ionic');
INSERT INTO date_skills VALUES (19, 'FRONTEND', 'SASS');
INSERT INTO date_skills VALUES (20, 'FRONTEND', 'Bootstrap');

INSERT INTO date_skills VALUES (21, 'DEV_OPS', 'Buddy');
INSERT INTO date_skills VALUES (22, 'DEV_OPS', 'Docker');
INSERT INTO date_skills VALUES (23, 'DEV_OPS', 'Kubernetes');
INSERT INTO date_skills VALUES (24, 'DEV_OPS', 'Basis');
INSERT INTO date_skills VALUES (25, 'DEV_OPS', 'TestRail');
INSERT INTO date_skills VALUES (26, 'DEV_OPS', 'QuerySurge');
INSERT INTO date_skills VALUES (27, 'DEV_OPS', 'Jenkins');
INSERT INTO date_skills VALUES (28, 'DEV_OPS', 'Vagrant');
INSERT INTO date_skills VALUES (29, 'DEV_OPS', 'Prometheus');
INSERT INTO date_skills VALUES (30, 'DEV_OPS', 'Puppet');

INSERT INTO date_skills VALUES (31, 'OTHER', 'Intellij IDEA');
INSERT INTO date_skills VALUES (32, 'OTHER', 'Eclipse');
INSERT INTO date_skills VALUES (33, 'OTHER', 'Visual Studio Code');
INSERT INTO date_skills VALUES (34, 'OTHER', 'Postman');
INSERT INTO date_skills VALUES (35, 'OTHER', 'GitHub');
INSERT INTO date_skills VALUES (36, 'OTHER', 'GitLab');
INSERT INTO date_skills VALUES (37, 'OTHER', 'Slack');
INSERT INTO date_skills VALUES (38, 'OTHER', 'Jira');
INSERT INTO date_skills VALUES (39, 'OTHER', 'Git');
INSERT INTO date_skills VALUES (40, 'OTHER', 'Atom');
INSERT INTO date_skills VALUES (41, 'OTHER', 'PyCharm');
INSERT INTO date_skills VALUES (42, 'OTHER', 'WebStorm');
INSERT INTO date_skills VALUES (43, 'OTHER', 'DataGrip');