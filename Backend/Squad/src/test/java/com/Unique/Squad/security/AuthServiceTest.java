package com.Unique.Squad.security;

import com.Unique.Squad.jwt.AuthResponse;
import com.Unique.Squad.jwt.JwtResponse;
import com.Unique.Squad.user.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static org.junit.Assert.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthServiceTest {

    @Autowired
    private AuthService authService;

    @Test
    public void shouldAuthenticateUserTest() {

        // given
        UserDto userDto = new UserDto("username", "password", Collections.emptyList());

        // when
        AuthResponse jwtResponse = authService.authenticateUser(userDto);

        // then
        assertNotNull(jwtResponse);
    }
}
