package com.Unique.Squad.jwt;

import com.Unique.Squad.user.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class JwtUtilsTest {

    @Autowired
    private JwtUtils jwtUtils;

    @Test
    public void generateTokenTest() {

        // given
        UserDto userDto = new UserDto("username", "password", Collections.emptyList());

        // when
        String token = jwtUtils.generateToken(userDto);
        String username = jwtUtils.getUsernameFromJWT(token);
        boolean valid = jwtUtils.validateToken(token);

        // then
        assertFalse(valid);
        assertNotNull(token);
        assertEquals(userDto.getUsername(), username);

    }
}
