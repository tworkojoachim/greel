package com.Unique.Squad.userProfile;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.ProfileNotExistsException;
import com.Unique.Squad.registration.RegistrationModel;
import com.Unique.Squad.user.UserServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserProfileServiceImplTest {

    @Autowired
    private UserProfileServiceImpl userProfileService;

    @Autowired
    private static UserServiceImpl userService;

    @Test
    public void saveUserProfileTest() {

        // given
        String username = "username";
        UserProfile userProfile = UserProfile.builder()
                .firstName("firstName")
                .lastName("lastName")
                .phoneNumber("324234324")
                .linkedin("linkedin")
                .gitHub("github")
                .socialStatus(UserProfile.SocialStatus.STUDENT)
                .position(UserProfile.Position.FULL_STACK)
                .experience(24L)
                .build();

        // when
        UserProfile result = userProfileService.save(userProfile, username);

        // then
        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getUser());

    }

    @Test(expected = ProfileNotExistsException.class)
    public void saveUserProfileFailTest() {

        // given
        String username = "usernameNotExist";
        UserProfile userProfile = UserProfile.builder()
                .firstName("firstName")
                .lastName("lastName")
                .phoneNumber("324234324")
                .linkedin("linkedin")
                .gitHub("github")
                .socialStatus(UserProfile.SocialStatus.STUDENT)
                .position(UserProfile.Position.FULL_STACK)
                .experience(24L)
                .build();

        // when
        UserProfile result = userProfileService.save(userProfile, username);

        // then exc ProfileNotExistsException
    }

    @Test(expected = BadRequestException.class)
    public void validUserProfileFirstNameTest() {

        // given
        UserProfile userProfile = UserProfile.builder()
                .firstName("")
                .lastName("lastName")
                .phoneNumber("324234324")
                .linkedin("linkedin")
                .gitHub("github")
                .socialStatus(UserProfile.SocialStatus.STUDENT)
                .position(UserProfile.Position.FULL_STACK)
                .experience(24L)
                .build();

        // when
        userProfileService.validUserProfile(userProfile);

        // then exc BadRequestException
    }

    @Test(expected = BadRequestException.class)
    public void validUserProfileLastNameTest() {

        // given
        UserProfile userProfile = UserProfile.builder()
                .firstName("firstName")
                .lastName("assssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss")
                .phoneNumber("324234324")
                .linkedin("linkedin")
                .gitHub("github")
                .socialStatus(UserProfile.SocialStatus.STUDENT)
                .position(UserProfile.Position.FULL_STACK)
                .experience(24L)
                .build();

        // when
        userProfileService.validUserProfile(userProfile);

        // then exc BadRequestException
    }

    @Test(expected = BadRequestException.class)
    public void validUserProfilePhoneNumberTest() {

        // given
        UserProfile userProfile = UserProfile.builder()
                .firstName("firstName")
                .lastName("lastName")
                .phoneNumber("324-324-4356-x")
                .linkedin("linkedin")
                .gitHub("github")
                .socialStatus(UserProfile.SocialStatus.STUDENT)
                .position(UserProfile.Position.FULL_STACK)
                .experience(24L)
                .build();

        // when
        userProfileService.validUserProfile(userProfile);

        // then exc BadRequestException
    }

    @Test
    public void getUserProfileTest() {

        // given
        String username = "username";

        // when
        UserProfileDto userProfile = userProfileService.getUserProfile(username);

        // then
        assertEquals("firstName", userProfile.getFirstName());
        assertEquals("lastName", userProfile.getLastName());
        assertEquals("324234324", userProfile.getPhoneNumber());
        assertEquals("linkedin", userProfile.getLinkedin());
        assertEquals("github", userProfile.getGitHub());
        assertEquals(UserProfile.Position.FULL_STACK, userProfile.getPosition());
        assertEquals(UserProfile.SocialStatus.STUDENT, userProfile.getSocialStatus());
        assertEquals(24L, userProfile.getExperience());

    }

    @Test
    public void getUserProfileByIdTest() {

        // given
        Long id = 1L;

        // when
        UserProfileDto userProfile = userProfileService.getUserProfileById(id);

        // then
        assertEquals("firstName", userProfile.getFirstName());
        assertEquals("lastName", userProfile.getLastName());
        assertEquals("324234324", userProfile.getPhoneNumber());
        assertEquals("linkedin", userProfile.getLinkedin());
        assertEquals("github", userProfile.getGitHub());
        assertEquals(UserProfile.Position.FULL_STACK, userProfile.getPosition());
        assertEquals(UserProfile.SocialStatus.STUDENT, userProfile.getSocialStatus());
        assertEquals(24L, userProfile.getExperience());
    }

}





