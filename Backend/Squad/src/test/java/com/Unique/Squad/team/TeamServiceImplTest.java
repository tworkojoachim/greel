package com.Unique.Squad.team;

import com.Unique.Squad.exceptions.BadRequestException;
import com.Unique.Squad.exceptions.NoTeamExceptions;
import com.Unique.Squad.user.User;
import com.Unique.Squad.user.UserInfoDto;
import com.Unique.Squad.user.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class TeamServiceImplTest {

    @Autowired
    private TeamServiceImpl teamService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldAddTeamTest() {

        // given
        String username = "username";
        Team newTeam = Team.builder()
                .name("TestName")
                .description("Amazing Team for amazing people!")
                .teamSize(4L)
                .projectDescription("Amazing project for amazing team!")
                .peopleSought("Amazing people with passion!")
                .build();

        // when
        Team team = teamService.addTeam(newTeam, username);

        // then
        assertNotNull(team.getId());
        assertEquals(1, team.getUserAdmins().size());
        assertEquals(1, team.getUsers().size());

    }

    @Test
    public void shouldDateTimeFormatTest() {

        // given
        LocalDate localDate = LocalDate.now();

        // when
        String data = teamService.format(localDate).toString();

        // then
        assertEquals(10, data.length());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void shouldGetUserFromDbTestNegative() {

        // given
        String username = "xD";

        // when
        teamService.getUserFromDb(username);

        // then exc UsernameNotFoundException
    }

    @Test
    public void shouldGetUserFromDbTestPositive() {

        // given
        String username = "username";

        // when
        User user = teamService.getUserFromDb(username);

        // then
        assertNotNull(user);
    }

    @Test(expected = NoTeamExceptions.class)
    public void shouldGetTeamByUserTestNegative() {

        // given
        User user = User.builder()
                .username("notExistUsername")
                .email("email@email.com.com")
                .password("password")
                .build();
        String username = "notExistUsername";

        // when
        userRepository.save(user);
        Team team = teamService.getTeamByUser(username);

        // then exc NoTeamExceptions
    }

    @Test(expected = NoTeamExceptions.class)
    public void shouldGetTeamByIdTestNegative() {

        // given
        Long id = 342L;

        // when
        teamService.getTeam(id);

        // then exc NoTeamExceptions
    }

    @Test
    public void shouldGetTeamByIdTestPositive() {

        // given
        Long id = 1L;

        // when
        Team team = teamService.getTeam(id);

        // then
        assertNotNull(team);
    }

    @Test
    public void shouldGetTeamInfoByIdTestPositive() {

        // given
        Long id = 1L;

        // when
        TeamInfoDto TeamInfoDto = teamService.getTeamInfoById(id);

        // then
        assertNotNull(TeamInfoDto);
    }

    @Test
    public void shouldExistsByUserAdminTest() {

        // given
        String username = "username2";

        // when
        boolean x = teamService.existsByUserAdmin(username);

        // then
        assertTrue(x);
    }

    @Test
    public void shouldExistsByUserTest() {

        // given
        String username = "username2";

        // when
        boolean x = teamService.existsByUser(username);

        // then
        assertTrue(x);
    }

    @Test
    public void shouldGetTeamInfoByUsernameTest() {

        // given
        String username = "username2";

        // when
        TeamInfoDto TeamInfoDto = teamService.getTeamInfoByUsername(username);

        // then
        assertNotNull(TeamInfoDto);
    }

    @Test
    public void shouldGetAllTeamUsersTest() {

        // given
        String username = "username2";

        // when
        Set<UserInfoDto> userInfoDtos = teamService.getAllTeamUsers(username);

        // then
        assertNotNull(userInfoDtos);
        assertEquals(1, userInfoDtos.size());
    }

    @Test(expected = BadRequestException.class)
    public void shouldGivePermissionsTestNegative() {

        // given
        String username = "username2";
        Long id = 2L;

        // when
        teamService.givePermissions(id, username);

        // then exc BadRequestException
    }

    @Test
    @Transactional
    public void shouldGivePermissionsTestPositive() {

        // given
        String username = "username3";
        Long id = 3L;

        // when
        teamService.givePermissions(id, username);
        boolean x = teamService.existsByUserAdmin(username);

        // then
        assertTrue(x);
    }

    @Test
    @Transactional
    public void shouldRemovePermissionsTestPositive() {

        // given
        String username = "username2";
        Long id = 2L;

        // when
        teamService.removePermissions(id, username);
        boolean x = teamService.existsByUserAdmin(username);

        // then
        assertFalse(x);
    }

    @Test(expected = BadRequestException.class)
    public void shouldRemovePermissionsTestNegative() {

        // given
        String username = "username";
        Long id = 1L;

        // when
        teamService.removePermissions(id, username);

        // then exc BadRequestException
    }

    @Test
    @Transactional
    public void shouldRemoveUserFromTeamTestPositive() {

        // given
        String username = "username3";
        Long id = 3L;

        // when
        teamService.removeUserFromTeam(id, username);
        boolean x = teamService.existsByUser(username);

        // then
        assertFalse(x);
    }

    @Test(expected = BadRequestException.class)
    public void shouldRemoveUserFromTeamTestNegative() {

        // given
        String username = "username";
        Long id = 1L;

        // when
        teamService.removeUserFromTeam(id, username);

        // then exc BadRequestException
    }

    @Test(expected = BadRequestException.class)
    public void shouldGetUserByIdTestNegative() {

        // given
        Long id = 324L;

        // when
        teamService.getUserById(id);

        // then exc BadRequestException
    }

    @Test(expected = BadRequestException.class)
    public void shouldGetTeamIdByUserTestNegative() {

        // given
        String username = "test";

        // when
        teamService.getTeamIdByUser(username);

        // then exc BadRequestException
    }


}