package com.Unique.Squad.skills.date;

import com.Unique.Squad.skills.dateSkills.DateSkills;
import com.Unique.Squad.skills.dateSkills.DateSkillsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class DateSkillsServiceImpl {

    @Autowired
    public com.Unique.Squad.skills.dateSkills.DateSkillsServiceImpl dateSkillsService;

    @Test
    public void shouldGetAllByCategoryTestOne() {

        // given
        String category = "FRONTEND";
        String username = "username";

        // when
        List<DateSkills> dateSkills = dateSkillsService.getAllByCategory(category, username);

        // then
        assertNotNull(dateSkills);
        assertEquals(10, dateSkills.size());
    }

    @Test
    public void shouldGetAllByCategoryTestTwo() {

        // given
        String category = "MySkills";
        String username = "username";

        // when
        List<DateSkills> dateSkills = dateSkillsService.getAllByCategory(category, username);

        // then
        assertEquals(1, dateSkills.size());
    }

    @Test
    public void shouldGetByNameTest() {

        // given
        String name = "Java";

        // when
        DateSkills dateSkills = dateSkillsService.getByName(name);

        // then
        assertNotNull(dateSkills);
        assertEquals("Java", dateSkills.getName());
    }
}
