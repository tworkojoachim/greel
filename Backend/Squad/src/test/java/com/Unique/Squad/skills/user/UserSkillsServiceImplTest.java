package com.Unique.Squad.skills.user;

import com.Unique.Squad.exceptions.FieldIsEmptyException;
import com.Unique.Squad.exceptions.SkillsAlreadyExistsException;
import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.skills.userSkills.UserSkills;
import com.Unique.Squad.skills.userSkills.UserSkillsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserSkillsServiceImplTest {

    @Autowired
    private UserSkillsServiceImpl userSkillsService;

    @Test
    public void shouldAddUserSkillTestPositive() {

        // given
        UserSkills userSkills = new UserSkills();
        userSkills.setName("C++");
        userSkills.setCategory(Skills.Category.BACKEND);
        userSkills.setLevel(Skills.Level.BEGINNER);
        userSkills.setNumberOfMonth(32L);

        String username = "username";

        // when
        UserSkills userSkillsDb = userSkillsService.addUserSkills(userSkills, username);

        // then
        assertNotNull(userSkillsDb.getId());
    }

    @Test(expected = FieldIsEmptyException.class)
    public void shouldAddUserSkillTestNegativeFirst() {

        // given
        UserSkills userSkills = new UserSkills();
        userSkills.setCategory(Skills.Category.BACKEND);
        userSkills.setLevel(Skills.Level.BEGINNER);
        userSkills.setNumberOfMonth(32L);

        String username = "username";

        // when
        UserSkills userSkillsDb = userSkillsService.addUserSkills(userSkills, username);

        // then exc FieldIsEmptyException
    }

    @Test(expected = SkillsAlreadyExistsException.class)
    public void shouldAddUserSkillTestNegativeSecond() {

        // given
        UserSkills userSkills = new UserSkills();
        userSkills.setName("Java");
        userSkills.setCategory(Skills.Category.BACKEND);
        userSkills.setLevel(Skills.Level.BEGINNER);
        userSkills.setNumberOfMonth(32L);

        String username = "username";

        // when
        UserSkills userSkillsDb = userSkillsService.addUserSkills(userSkills, username);

        // then exc SkillsAlreadyExistsException
    }

    @Test(expected = UsernameNotFoundException.class)
    public void shouldAddUserSkillTestNegativeThird() {

        // given
        UserSkills userSkills = new UserSkills();
        userSkills.setName("Java");
        userSkills.setCategory(Skills.Category.BACKEND);
        userSkills.setLevel(Skills.Level.BEGINNER);
        userSkills.setNumberOfMonth(32L);

        String username = "usernameXd";

        // when
        UserSkills userSkillsDb = userSkillsService.addUserSkills(userSkills, username);

        // then exc UsernameNotFoundException
    }

    @Test
    public void shouldGetAllUserSkillsTest() {

        // given
        String username = "username";

        // when
        List<SkillsDto> skillsDtoList = userSkillsService.getAllUserSkills(username);

        // then
        assertNotNull(skillsDtoList);
        assertEquals(1, skillsDtoList.size());

    }

    @Test
    public void shouldGetAllUserSkillsByIdTest() {

        // given
        Long id = 1L;

        // when
        List<SkillsDto> skillsDtoList = userSkillsService.getAllUserSkillsById(id);

        // then
        assertNotNull(skillsDtoList);
        assertEquals(2, skillsDtoList.size());

    }

    @Test
    public void shouldDeleteUserSkillById() {

        // given
        Long skillId = 1L;

        // when
        userSkillsService.deleteUserSkill(skillId);
        List<SkillsDto> skillsDtoList = userSkillsService.getAllUserSkillsById(1L);

        // then
        assertNotNull(skillsDtoList);
        assertEquals(1, skillsDtoList.size());
    }

}
