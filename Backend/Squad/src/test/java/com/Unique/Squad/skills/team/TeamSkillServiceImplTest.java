package com.Unique.Squad.skills.team;

import com.Unique.Squad.exceptions.FieldIsEmptyException;
import com.Unique.Squad.exceptions.NoTeamExceptions;
import com.Unique.Squad.exceptions.SkillsAlreadyExistsException;
import com.Unique.Squad.skills.Skills;
import com.Unique.Squad.skills.SkillsDto;
import com.Unique.Squad.skills.teamSkills.SearchModel;
import com.Unique.Squad.skills.teamSkills.TeamSearchModel;
import com.Unique.Squad.skills.teamSkills.TeamSkills;
import com.Unique.Squad.skills.teamSkills.TeamSkillsServiceImpl;
import com.Unique.Squad.team.Team;
import com.Unique.Squad.team.TeamServiceImpl;
import io.jsonwebtoken.lang.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class TeamSkillServiceImplTest {

    @Autowired
    private TeamSkillsServiceImpl teamService;

    @Test
    @Transactional
    public void shouldAddTeamSkill() {

        // given
        TeamSkills teamSkills = new TeamSkills();
        teamSkills.setName("Java");
        teamSkills.setCategory(Skills.Category.BACKEND);
        teamSkills.setLevel(Skills.Level.ADVANCED);
        String username = "username2";

        // when
        TeamSkills saved = teamService.addSkill(teamSkills, username);

        // then
        assertNotNull(saved.getId());
    }

    @Test(expected = FieldIsEmptyException.class)
    public void shouldAddTeamSkillFailedEmptyFields() {

        // given
        TeamSkills teamSkills = new TeamSkills();
        String username = "username2";

        // when
       teamService.addSkill(teamSkills, username);

       // then exc FieldIsEmptyException
    }

    @Transactional
    @Test(expected = SkillsAlreadyExistsException.class)
    public void shouldAddTeamSkillFailedAlreadyExists(){

        // given
        TeamSkills teamSkills = new TeamSkills();
        teamSkills.setName("C++");
        teamSkills.setCategory(Skills.Category.BACKEND);
        teamSkills.setLevel(Skills.Level.ADVANCED);
        String username = "username2";

        // when
        teamService.addSkill(teamSkills, username);

        // then exc SkillsAlreadyExistsException
    }

    @Test
    public void shouldGetAllByTeamTest() {

        // given
        String username = "username2";

        // when
        List<SkillsDto> teamSkills = teamService.getAllByTeam(username);

        // then
        assertNotNull(teamSkills);
        assertEquals(1, teamSkills.size());
    }

    @Test
    public void shouldGetAllByTeamIdTest() {

        // given
        Long id = 1L;

        // when
        Set<SkillsDto> teamSkills = teamService.getAllByTeamId(id);

        // then
        assertNotNull(teamSkills);
        assertEquals(1, teamSkills.size());
    }

    @Test
    public void shouldDeleteTeamSkillTest() {

        // given
        Long id = 1L;

        // when
        teamService.deleteSkill(id);
        Set<SkillsDto> teamSkills = teamService.getAllByTeamId(id);

        // then
        assertNotNull(teamSkills);
        assertEquals(1, teamSkills.size());
    }

    @Test
    public void shouldFindTeamByUsernameTest() {

        // given
        String username = "username2";

        // when
        Team team = teamService.findTeamByUsername(username);

        // then
        assertNotNull(team);
    }

    @Test(expected = NoTeamExceptions.class)
    public void shouldFindTeamByUsernameTestFailed() {

        // given
        String username = "usernameXd";

        // when
        Team team = teamService.findTeamByUsername(username);

        // then exc NoTeamExceptions
    }

    @Test
    public void shouldGetAllSearchTeamsTest() {

        // given
        SearchModel searchModel = SearchModel.builder()
                .sizeTeam(5L)
                .category("BACKEND")
                .needsList(Arrays.asList("Java", "C++", "C"))
                .build();

        // when
        Set<SkillsDto> teamSkills = teamService.getAllByTeamId(1L);
        List<TeamSearchModel> teamSearchModels = teamService.getAllSearchTeams(searchModel);

        // then
        assertNotNull(teamSearchModels);
        assertEquals(1, teamSearchModels.size());
    }

    @Test
    public void shouldCategoryFormTest() {

        // given
        List<String> list = Arrays.asList("BACKEND", "FRONTEND", "DEV_OPS", "OTHER", "MySkills");

        // when // then
        list.forEach(value -> teamService.categoryForm(value));
    }

}











