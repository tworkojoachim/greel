package com.Unique.Squad.userProfile;

import com.Unique.Squad.jwt.AuthResponse;
import com.Unique.Squad.jwt.JwtResponse;
import com.Unique.Squad.security.AuthService;
import com.Unique.Squad.user.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.beans.factory.annotation.Autowired;
import static org.hamcrest.Matchers.containsString;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserProfileControllerTest {

    @Autowired
    private UserProfileController userProfileController;

    @Autowired
    private AuthService authService;

    @Autowired
    private MockMvc mockMvc;

    private static AuthResponse token;

    private static boolean tokenLoaded = false;

    @Before
    public void setUp() throws Exception {
        if (!tokenLoaded) {
            token = authService.authenticateUser(new UserDto("username", "password", Collections.emptyList()));
            tokenLoaded = true;
        }
    }

    @Test
    public void shouldGetUserProfileTest() throws Exception {
        mockMvc.perform(get("/profile")
                .header("Authorization", token.getToken()))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("firstName"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("lastName"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gitHub").value("github"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value("324234324"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.position").value("FULL_STACK"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.socialStatus").value("STUDENT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.experience").value(24));

    }


}
