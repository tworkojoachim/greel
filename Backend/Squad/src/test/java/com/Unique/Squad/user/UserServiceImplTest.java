package com.Unique.Squad.user;

import com.Unique.Squad.exceptions.*;
import com.Unique.Squad.registration.RegistrationModel;
import com.Unique.Squad.role.Role;
import com.Unique.Squad.role.RoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceImplTest {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void registrationTest() {

        // given
        RegistrationModel registrationModel = RegistrationModel.builder()
                .username("testUsernameModel")
                .email("testEmailModel")
                .password("TestPassword1!")
                .confirmPassword("TestPassword1!")
                .build();

        // when
        userService.registration(registrationModel);

    }

    @Test(expected = FieldIsEmptyException.class)
    public void userValidationByUsernameEmptyTest() {

        // given
        String username = "";

        // when
        userService.userValidationByUsername(username);

        // then exc FieldIsEmptyException
    }

    @Test(expected = NameAlreadyExistsException.class)
    public void userValidationByUsernameAlreadyExistTest() {

        // given
        String username = "testUsername";
        User user = User.builder()
                .username("testUsername")
                .password("password")
                .email("email")
                .roles(Collections.singleton(roleRepository.findByName(Role.Name.USER)))
                .status(User.Status.ACTIVE)
                .userProfile(null)
                .build();

        // when
        userRepository.save(user);
        userService.userValidationByUsername(username);

        // then exc NameAlreadyExistsException
    }

    @Test(expected = FieldIsEmptyException.class)
    public void userValidationByEmailEmptyException() {

        // given
        String email = "";

        // when
        userService.userValidationByEmail(email);

        // then exc FieldIsEmptyException
    }

    @Test(expected = NameAlreadyExistsException.class)
    public void userValidationByEmailAlreadyExistTest() {

        // given
        String email = "testEmail";
        User user = User.builder()
                .username("aaa")
                .password("password")
                .email("testEmail")
                .roles(Collections.singleton(roleRepository.findByName(Role.Name.USER)))
                .status(User.Status.ACTIVE)
                .userProfile(null)
                .build();

        // when
        userRepository.save(user);
        userService.userValidationByEmail(email);

        // then exc NameAlreadyExistsException
    }

    @Test(expected = PasswordNotMatchException.class)
    public void validationPasswordNotMatchTest() {

        // given
        RegistrationModel registrationModel = RegistrationModel.builder()
                .username("testUsernamePass")
                .email("testEmailPass")
                .password("Good pass")
                .confirmPassword("Wrong pass")
                .build();

        // when
        userService.validation(registrationModel);

        // then exc PasswordNotMatchException
    }

    @Test(expected = PasswordException.class)
    public void validationPasswordTest() {

        // given
        RegistrationModel registrationModel = RegistrationModel.builder()
                .username("testUsername1")
                .email("testEmail1")
                .password("thisSame")
                .confirmPassword("thisSame")
                .build();

        // when
        userService.validation(registrationModel);

        // then exc PasswordException
    }
}
