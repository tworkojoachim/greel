package com.Unique.Squad.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class LoadUserServiceTest {

    @Autowired
    private LoadUserService loadUserService;

    @Test
    public void loadUserByUsernameSuccessTest() {

        // given
        String username = "username";

        // when
        UserDetails userDetails = loadUserService.loadUserByUsername(username);

        // then
        assertNotNull(userDetails.getAuthorities());
        assertNotNull(userDetails.getPassword());
        assertEquals(username, userDetails.getUsername());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsernameFailedTest() {

        // given
        String username = "usernameNotFound";

        // when
        UserDetails userDetails = loadUserService.loadUserByUsername(username);

        // then exc UsernameNotFoundException

    }
}
