//package com.Unique.Squad.user;
//
//import com.Unique.Squad.registration.RegistrationModel;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import static org.junit.Assert.assertEquals;
//
//@ActiveProfiles("test")
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class UserControllerTest {
//
//    @Autowired
//    private TestRestTemplate testRestTemplate;
//
//    @Autowired
//    private UserService userService;
//
//    @LocalServerPort
//    private int port;
//
//    @Test
//    public void shouldRegistrationTest()  {
//
//        // given
//        RegistrationModel registrationModel = RegistrationModel.builder()
//                .username("aaaaaabbbb")
//                .email("aaaaa@aaaaa.com")
//                .password("Password1!")
//                .confirmPassword("Password1!").build();
//
//        HttpHeaders headers = new HttpHeaders();
//        HttpEntity<RegistrationModel> request = new HttpEntity<>(registrationModel, headers);
//
//        // when
//        ResponseEntity<String> result = this.testRestTemplate.postForEntity("http://localhost:" + port + "/registration", request, String.class);
//
//
//        // then
//        assertEquals(200, result.getStatusCodeValue());
//        assertEquals("Registration complete!", result.getBody());
//
//    }
//
//    @Test
//    public void shouldAuthorizationSuccessfully() {
//
//        // given
//        UserDto userDto = new UserDto("username", "password", null);
//
//        // when
//        ResponseEntity<JwtResponse> result = this.testRestTemplate.postForEntity("http://localhost:" + port + "/authorization", userDto, JwtResponse.class);
//
//        // then
//        assertNotNull(result);
//        assertNotNull(Objects.requireNonNull(result.getBody()).getToken());
//        assertEquals(result.getStatusCode().value(), 200 );
//    }
//
//}
