import { Component, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';


@Component({
  selector: 'app-context',
  templateUrl: './context.component.html',
  styleUrls: ['./context.component.css']
})
export class ContextComponent implements OnInit {

  fontClass = '';
  imgSrc = '';

  constructor(public mediaObserver: MediaObserver,
              public breakpointObserver: BreakpointObserver) { }

  checkSize(size: string): boolean {
    if (this.mediaObserver.isActive(size)) {
      return false;
    } else { return true; }

  }

  ngOnInit() {
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ]).subscribe(result => {
      if (result.breakpoints[Breakpoints.XSmall]) {
       this.imgSrc = 'xs';
       this.fontClass = 'Small';
      }
      if (result.breakpoints[Breakpoints.Small]) {
        this.imgSrc = 'sm';
        this.fontClass = 'Small';
      }
      if (result.breakpoints[Breakpoints.Medium]) {
        this.imgSrc = 'md';
        this.fontClass = 'Big';
      }
      if (result.breakpoints[Breakpoints.Large]) {
        this.imgSrc = 'lg';
        this.fontClass = 'Big';
      }
      if (result.breakpoints[Breakpoints.XLarge]) {
        this.imgSrc = 'xl';
        this.fontClass = 'Big';
      }
    });
  }
}
