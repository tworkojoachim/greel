import { Component, OnInit } from '@angular/core';
import {TeamService} from '../service/team/teamService';
import {Team} from '../models/team';
import {Form, NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {NotificationService} from '../service/notification/notification.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  constructor(private router: Router,
              private teamService: TeamService,
              private notification: NotificationService) {
  }

  validate: boolean;
  haveTeam: boolean;
  team: Team = new Team();

  ngOnInit(): void {
    this.teamService.haveTeam().subscribe(data => {
      this.haveTeam = JSON.parse(data);
      if (this.haveTeam === true)
        this.router.navigate(['team/information']);
      else
        this.validate = JSON.parse(localStorage.getItem('isProfile'));
    });
  }

  addTeam() {
    this.teamService.addTeam(this.team,).subscribe(data => {
      this.teamService.haveTeam().subscribe(xd => {
        this.haveTeam = JSON.parse(xd);
        this.notification.displaySuccessNotification('Successfully added team!');
        this.router.navigate(['team/information']);
      });
    }, error => {
        this.notification.displayErrorNotification(JSON.parse(error.error).message);
    });
  }
}
