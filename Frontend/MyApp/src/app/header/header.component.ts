import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../service/auth/authentication.service';
import {Router} from '@angular/router';
import {NotificationService} from '../service/notification/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private loginService: AuthenticationService,
              private router: Router,
              private notification: NotificationService) { }


  ngOnInit(): void {
  }

  isUserLogIn() {
    return this.loginService.isUserLoggedIn();
  }

  logout(){
    this.loginService.logOut().subscribe( data => {
      this.loginService.clearSession('Successfully logged out');
    });
  }

}
