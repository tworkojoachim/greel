import { Component, OnInit } from '@angular/core';
import {ApplicationService} from '../../service/application/applicationService';
import {NotificationService} from '../../service/notification/notification.service';
import {TeamService} from '../../service/team/teamService';
import {ApplicationDto} from '../../models/application/applicationDto';
import {TeamInfoDto} from '../../models/teamInfoDto';

@Component({
  selector: 'app-user-applications',
  templateUrl: './user-applications.component.html',
  styleUrls: ['./user-applications.component.css']
})
export class UserApplicationsComponent implements OnInit {

  constructor(private serviceApp: ApplicationService,
              private notification: NotificationService,
              private teamService: TeamService) { }

  applications: ApplicationDto[];
  isEmpty: boolean;
  teamModel: TeamInfoDto = new TeamInfoDto();

  ngOnInit(): void {
    this.serviceApp.getAllUserApplications().subscribe(data => {
        this.applications = data;
        this.isEmpty = this.applications.length === 0;
      },
      error => this.notification.displayErrorNotification('Something went wrong try again!'))
  }

  getTeamInfo(teamId: number) {
    this.teamService.getTeamById(teamId).subscribe(data => {
      this.teamModel = JSON.parse(data);
    })
  }

  cancelApplication(application: ApplicationDto) {
    this.serviceApp.discardApplicationUser(application).subscribe( x => {
        this.applications = this.applications.filter(obj => obj !== application);
        this.notification.displaySuccessNotification('The application has been rejected!')
      },
      error => this.notification.displayErrorNotification(error.error.message));
  }

}
