import { Component, OnInit } from '@angular/core';
import {ApplicationService} from '../../service/application/applicationService';
import {NotificationService} from '../../service/notification/notification.service';
import {ApplicationDto} from '../../models/application/applicationDto';
import {TeamService} from '../../service/team/teamService';
import {TeamInfoDto} from '../../models/teamInfoDto';

@Component({
  selector: 'app-user-invitations',
  templateUrl: './user-invitations.component.html',
  styleUrls: ['./user-invitations.component.css']
})
export class UserInvitationsComponent implements OnInit {

  constructor(private serviceApp: ApplicationService,
              private notification: NotificationService,
              private teamService: TeamService) { }

  applications: ApplicationDto[];
  isEmpty: boolean;
  teamModel: TeamInfoDto = new TeamInfoDto();

  ngOnInit(): void {
    this.serviceApp.getAllUserInvitations().subscribe(data => {
      this.applications = data;
      this.isEmpty = this.applications.length === 0;
    },
      error => this.notification.displayErrorNotification('Something went wrong try again!'))
  }

  getTeamInfo(teamId: number) {
    this.teamService.getTeamById(teamId).subscribe(data => {
      this.teamModel = JSON.parse(data);
    })
  }

  applyApplication(application: ApplicationDto) {
    this.serviceApp.applyApplicationUser(application).subscribe( x => {
        this.applications = this.applications.filter(obj => obj !== application);
        this.notification.displaySuccessNotification('You have been added to the team!')
      },
      error => this.notification.displayErrorNotification(error.error.message));
  }

  discardApplication(application: ApplicationDto) {
    this.serviceApp.discardApplicationUser(application).subscribe( x => {
        this.applications = this.applications.filter(obj => obj !== application);
        this.notification.displaySuccessNotification('The application has been rejected!')
      },
      error => this.notification.displayErrorNotification(error.error.message));
  }

}
