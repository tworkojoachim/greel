import {Component, ErrorHandler, OnInit, ViewChild} from '@angular/core';
import { RegistrationService } from '../service/registration/registration.service';
import { User } from '../service/auth/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import {NotificationService} from '../service/notification/notification.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  @ViewChild('registrationForm')
  regForm: NgForm;

  user: User = new User('', '', '', '', '');
  message: string;

  constructor(private router: Router,
              private service: RegistrationService,
              private notification: NotificationService) { }

  ngOnInit(): void {
  }

  registerNow() {
    this.service.doRegistration(this.user).subscribe(data => {
      this.message = null;
      this.router.navigate(['login']);
      this.notification.displaySuccessNotification('The account has been created');
    }, error => {
       this.message = error.error.message;
        this.notification.displayErrorNotification('Something went wrong :(');
      });
  }

}
