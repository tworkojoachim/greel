import { Component, OnInit } from '@angular/core';
import { UserProfile } from '../models/userProfile';
import { UserProfileService } from '../service/userProfile/userProfileService';
import {NotificationService} from '../service/notification/notification.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private profileService: UserProfileService,
              private notification: NotificationService) { }

  profile: UserProfile = new UserProfile();

  ngOnInit(): void {
    this.profileService.getData().subscribe( data => {
      this.profile = data;
    });
  }

  saveProfile() {
    if(this.valid()) {
      this.profileService.saveUserProfile(this.profile).subscribe(data => {
          this.profile = data;
          this.notification.displaySuccessNotification('Profile updated successfully!');
          localStorage.setItem('isProfile', JSON.stringify(true));
        }, error => {
          this.notification.displayErrorNotification(error.error.message);
        }
      );
    }
  }

  valid() {
    return this.profile.firstName != null &&
      this.profile.lastName != null &&
      this.profile.experience != null;
  }

}
