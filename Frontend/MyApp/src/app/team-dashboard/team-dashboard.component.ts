import { Component, OnInit } from '@angular/core';
import {TeamService} from '../service/team/teamService';

@Component({
  selector: 'app-team-dashboard',
  templateUrl: './team-dashboard.component.html',
  styleUrls: ['./team-dashboard.component.css']
})
export class TeamDashboardComponent implements OnInit {

  constructor(private teamService: TeamService) { }

  isAdmin: boolean;

  ngOnInit(): void {
    this.teamService.isAdmin().subscribe(data => {
      this.isAdmin = JSON.parse(data);
    });
  }

  invitations() {
    localStorage.setItem('invite', JSON.stringify(true));
  }


}
