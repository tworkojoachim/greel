import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BasicAuthHttpInterceptorService } from './service/auth/basic-auth-http-interceptor.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContextComponent } from './context/context.component';
import { ButtonsDirective } from './shared/buttons.directive';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { RegistrationComponent } from './registration/registration.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProfileComponent } from './profile/profile.component';
import { SkillsComponent } from './skills/skills.component';
import { TeamComponent } from './team/team.component';
import { TeamDashboardComponent } from './team-dashboard/team-dashboard.component';
import { InformationComponent } from './team-dashboard-content/information/information.component';
import { MemberShipComponent } from './team-dashboard-content/member-ship/member-ship.component';
import { ApplicationsComponent } from './team-dashboard-content/applications/applications.component';
import { TidingsComponent } from './team-dashboard-content/tidings/tidings.component';
import { SearchComponent } from './search/search.component';
import { TeamNeedsComponent } from './team-needs/team-needs.component';
import { SearchPeopleComponent } from './search/search-people/search-people.component';
import { SearchTeamComponent } from './search/search-team/search-team.component';
import { ApplicationComponent } from './application/application.component';
import {AuthGuardService} from './service/auth/auth-guard.service';
import {PhonePipe} from './shared/pipe/phonePipe';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserApplicationsComponent } from './user-dashboard-content/user-applications/user-applications.component';
import { UserInvitationsComponent } from './user-dashboard-content/user-invitations/user-invitations.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ContextComponent,
    ButtonsDirective,
    RegistrationComponent,
    ProfileComponent,
    SkillsComponent,
    TeamComponent,
    TeamDashboardComponent,
    InformationComponent,
    MemberShipComponent,
    ApplicationsComponent,
    TidingsComponent,
    SearchComponent,
    TeamNeedsComponent,
    SearchPeopleComponent,
    SearchTeamComponent,
    ApplicationComponent,
    PhonePipe,
    UserDashboardComponent,
    UserApplicationsComponent,
    UserInvitationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: BasicAuthHttpInterceptorService, multi: true
    },
    {
      provide: AuthGuardService
    },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500, horizontalPosition: 'center', verticalPosition: 'bottom' }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
