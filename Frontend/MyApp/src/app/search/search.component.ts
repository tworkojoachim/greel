import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../service/notification/notification.service';
import {Router} from '@angular/router';
import {TeamService} from '../service/team/teamService';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private notifications: NotificationService,
              private teamService: TeamService,
              private router: Router) { }
  profile: boolean;
  skills: boolean;
  haveTeam: boolean;

  ngOnInit(): void {
    this.profile = JSON.parse(localStorage.getItem('isProfile'))
    this.skills = JSON.parse(localStorage.getItem('isSkill'))
    this.noTeam();
  }

  goTeam() {
    this.valid();
    if(this.profile && this.skills)
      this.router.navigate((['group']));
  }

  goPeople() {
    this.valid();
    if(!this.haveTeam)
      this.notifications.displayInfoNotification('You must have a team!')
    if(this.profile && this.skills && this.haveTeam)
      this.router.navigate((['people']));
  }

  valid() {
    if(!this.profile)
      this.notifications.displayInfoNotification('You must complete your profile!')
    else if(!this.skills)
      this.notifications.displayInfoNotification('You must complete your skills!')
  }

  noTeam() {
    this.teamService.haveTeam().subscribe(data => {
      this.haveTeam = JSON.parse(data);
    });
  }

}
