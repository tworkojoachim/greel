import { Component, OnInit } from '@angular/core';
import {DataSkillsService} from '../../service/skills/dataSkillsService';
import {DateSkills} from '../../models/dateSkills';
import {FormControl, NgForm} from '@angular/forms';
import {TeamSearchModel} from '../../models/search/teamSearchModel';
import {TeamSearch} from '../../models/teamSearch';
import {SearchService} from '../../service/search/searchService';
import {ActivatedRoute, Router} from '@angular/router';
import {TeamSkillsService} from '../../service/skills/teamSkillsService';
import {Skills} from '../../models/skills';

@Component({
  selector: 'app-search-team',
  templateUrl: './search-team.component.html',
  styleUrls: ['./search-team.component.css']
})
export class SearchTeamComponent implements OnInit {

  constructor(private dataSkills: DataSkillsService,
              private skillService: TeamSkillsService,
              private searchService: SearchService,
              private router: Router) { }

  category: string;
  skillsName: DateSkills[];
  skillName = new FormControl();
  result: TeamSearch[] = [];
  teamSearchModel: TeamSearchModel = new TeamSearchModel();
  errorMessage = '';
  teamSkillRequiredList: Skills[];

  isEmpty: boolean;
  notFound: boolean;

  ngOnInit(): void {
  }

  newData($event: any) {
    this.dataSkills.getAllSkillsByCategory(this.category).subscribe( data => {
      this.skillsName = JSON.parse(data);
    });
  }

  search(form: NgForm) {
    this.teamSearchModel.category = this.category;
    this.teamSearchModel.needsList = this.skillName.value;
    this.searchService.getAllSearchTeams(this.teamSearchModel).subscribe( data => {
      this.result = JSON.parse(data);
      this.isEmpty = this.result.length !== 0;
      this.notFound = !this.isEmpty;
      this.errorMessage = '';
    }, error => {
      this.errorMessage = JSON.parse(error.error).message;
    });
  }

  getRequiredSkills(id: number) {
    this.skillService.getAllTeamSkillsById(id).subscribe( data => {
      this.teamSkillRequiredList = JSON.parse(data);
      console.log(JSON.parse(data));
    });
  }

}
