import { Component, OnInit } from '@angular/core';
import {PeopleSearchModel} from '../../models/search/people/peopleSearchModel';
import {DataSkillsService} from '../../service/skills/dataSkillsService';
import {DateSkills} from '../../models/dateSkills';
import {FormControl, NgForm} from '@angular/forms';
import {TeamSearch} from '../../models/teamSearch';
import {UserProfile} from '../../models/userProfile';
import {SearchService} from '../../service/search/searchService';
import {SkillsService} from '../../service/skills/skillsService';
import {Skills} from '../../models/skills';
import {NotificationService} from '../../service/notification/notification.service';

@Component({
  selector: 'app-search-people',
  templateUrl: './search-people.component.html',
  styleUrls: ['./search-people.component.css']
})
export class SearchPeopleComponent implements OnInit {

  constructor(private dataSkills: DataSkillsService,
              private searchService: SearchService,
              private skillsService: SkillsService,
              private notification: NotificationService) { }

  searchPeopleModel: PeopleSearchModel = new PeopleSearchModel([], '', 0);
  category: string;
  skillsName: DateSkills[];
  skillName = new FormControl();
  result: UserProfile[] = [];
  errorMessage = '';
  userSkills: Skills[];

  isEmpty: boolean;
  notFound: boolean;

  ngOnInit(): void {
  }

  newData($event: any) {
    this.dataSkills.getAllSkillsByCategory(this.category).subscribe( data => {
      this.skillsName = JSON.parse(data);
    });
  }

  search(form: NgForm) {
    this.searchPeopleModel.category = this.category;
    this.searchPeopleModel.needsList = this.skillName.value;
    this.searchService.getAllSearchPeople(this.searchPeopleModel).subscribe( data => {
      this.result = data;
      this.isEmpty = this.result.length !== 0;
      this.notFound = !this.isEmpty;
      this.errorMessage = '';
    }, error => {
      this.errorMessage = error.error.message;
    });
  }

  getSkills(id: number) {
    this.skillsService.getAllUserSkillsById(id).subscribe(data => {
      this.userSkills = JSON.parse(data);
    }, error => this.notification.displayErrorNotification('Something went wrong!'));
  }

  round(value: number) {
    const x = Math.round(value / 12 - 0.5);
    if(x === 0) return ''
    else return x.toString() + ' years';
  }

  invitePeople() {
    localStorage.setItem('searchPeople', JSON.stringify(true));
  }
}
