import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {TeamService} from '../service/team/teamService';
import {TeamInfoDto} from '../models/teamInfoDto';
import {Application} from '../models/application/application';
import {ApplicationService} from '../service/application/applicationService';
import {NotificationService} from '../service/notification/notification.service';


@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {

  id: number;
  team: TeamInfoDto = new TeamInfoDto();
  application: Application = new Application();
  pom: boolean;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private teamService: TeamService,
              private applicationService: ApplicationService,
              private notification: NotificationService) {}


  ngOnInit(): void {
    this.pom = JSON.parse(localStorage.getItem('searchPeople'));
    this.id = +this.route.snapshot.paramMap.get('id');
    if(!this.pom) {
      this.teamService.getTeamById(this.id).subscribe(data => {
        this.team = JSON.parse(data);
      });
    } else
      this.teamService.getTeam().subscribe(data => {
        this.team = JSON.parse(data);
      })
  }

  sendApplication() {
    if(!this.pom)
      this.teamApplication();
    else
      this.peopleApplication();
  }

  teamApplication() {
    this.application.teamId = this.id;
    this.applicationService.sendApplicationTeam(this.application).subscribe(
      empty => {
        this.notification.displaySuccessNotification('The application has been sent successfully!')
        this.router.navigate(['search'])
      },
      error => {
        this.notification.displayErrorNotification(error.error.message);
      });
  }

  peopleApplication() {
    this.application.teamId = this.id;
    this.applicationService.sendApplicationPeople(this.application).subscribe(
      empty => {
        this.notification.displaySuccessNotification('The application has been sent successfully!')
        localStorage.removeItem('searchPeople');
        this.router.navigate(['search'])
      },
      error => this.notification.displayErrorNotification(error.error.message))
  }

}
