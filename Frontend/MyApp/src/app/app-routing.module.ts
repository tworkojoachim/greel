import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './service/auth/auth-guard.service';
import { RegistrationComponent } from './registration/registration.component';
import { ContextComponent } from './context/context.component';
import { ProfileComponent } from './profile/profile.component';
import { SkillsComponent } from './skills/skills.component';
import {TeamComponent} from './team/team.component';
import {InformationComponent} from './team-dashboard-content/information/information.component';
import {ApplicationsComponent} from './team-dashboard-content/applications/applications.component';
import {MemberShipComponent} from './team-dashboard-content/member-ship/member-ship.component';
import {TidingsComponent} from './team-dashboard-content/tidings/tidings.component';
import {SearchComponent} from './search/search.component';
import {TeamNeedsComponent} from './team-needs/team-needs.component';
import {SearchTeamComponent} from './search/search-team/search-team.component';
import {SearchPeopleComponent} from './search/search-people/search-people.component';
import {ApplicationComponent} from './application/application.component';
import {UserDashboardComponent} from './user-dashboard/user-dashboard.component';
import {UserApplicationsComponent} from './user-dashboard-content/user-applications/user-applications.component';
import {UserInvitationsComponent} from './user-dashboard-content/user-invitations/user-invitations.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'context', component: ContextComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
  { path: 'skills', component: SkillsComponent, canActivate: [AuthGuardService] },
  { path: 'team', component: TeamComponent,
    children: [
      { path: 'information', component: InformationComponent, canActivate: [AuthGuardService] },
      { path: 'applications', component: ApplicationsComponent, canActivate: [AuthGuardService] },
      { path: 'invitations', component: ApplicationsComponent, canActivate: [AuthGuardService] },
      { path: 'memberShip', component: MemberShipComponent, canActivate: [AuthGuardService] },
      { path: 'tidings', component: TidingsComponent, canActivate: [AuthGuardService] },
      { path: 'needs', component: TeamNeedsComponent, canActivate: [AuthGuardService] },
    ]
  },
  { path: 'menu', component: UserDashboardComponent,
    children: [
      { path: 'applications', component: UserApplicationsComponent, canActivate: [AuthGuardService] },
      { path: 'invitations', component: UserInvitationsComponent, canActivate: [AuthGuardService] },
    ]
  },
  { path: 'search', component: SearchComponent, canActivate: [AuthGuardService] },
  { path: 'group', component: SearchTeamComponent, canActivate: [AuthGuardService] },
  { path: 'application/:id', component: ApplicationComponent, canActivate: [AuthGuardService] },
  { path: 'people', component: SearchPeopleComponent, canActivate: [AuthGuardService] },
  { path: '', redirectTo: 'context', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
