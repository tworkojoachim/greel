import {Component, Input, OnInit} from '@angular/core';
import {UserProfile} from '../../models/userProfile';
import {TeamService} from '../../service/team/teamService';
import {MatDialog} from '@angular/material/dialog';
import {NotificationService} from '../../service/notification/notification.service';

@Component({
  selector: 'app-member-ship',
  templateUrl: './member-ship.component.html',
  styleUrls: ['./member-ship.component.css']
})
export class MemberShipComponent implements OnInit {

  constructor(private teamService: TeamService,
              public dialog: MatDialog,
              private notification: NotificationService) { }

  isEmpty: boolean;
  isAdmin: boolean;
  newList: UserProfile[];

  ngOnInit(): void {
    this.teamService.getAllTeamUsers().subscribe( data => {
      this.newList = data;
      this.isEmpty = this.newList === null || this.newList.length === 0;
      console.log(this.isEmpty)
    });
    this.teamService.isAdmin().subscribe(data => {
      this.isAdmin = JSON.parse(data);
    });
  }

  deleteFromTeam(id: number) {
    this.teamService.removeUser(id).subscribe( data => {
      this.newList = this.newList.filter((obj => obj.id !== id));
      this.notification.displaySuccessNotification('Successfully deleted user from team!')
    }, error => this.notification.displayErrorNotification(error.error.message));
  }

  addPermission(id: number) {
    this.teamService.addPermission(id).subscribe( data => {
      this.notification.displaySuccessNotification('Administrator privileges granted!');
      this.ngOnInit();
    }, error => this.notification.displayErrorNotification(error.error.message));
  }

  removePermission(id: number) {
    this.teamService.removePermission(id).subscribe( data => {
      this.notification.displaySuccessNotification('Administrator privileges removed!');
      this.ngOnInit();
    }, error => this.notification.displayErrorNotification(error.error.message));
  }

}
