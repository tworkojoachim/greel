import { Component, OnInit } from '@angular/core';
import {Team} from '../../models/team';
import {TeamInfoDto} from '../../models/teamInfoDto';
import {TeamService} from '../../service/team/teamService';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

  constructor(private teamService: TeamService) { }

  teamInfo: TeamInfoDto = new TeamInfoDto();

  ngOnInit(): void {
    this.teamService.getTeam().subscribe( data => {
      this.teamInfo = JSON.parse(data);
    });
  }

}
