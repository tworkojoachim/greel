import { Component, OnInit } from '@angular/core';
import {MessageService} from '../../service/message/message.service';

@Component({
  selector: 'app-tidings',
  templateUrl: './tidings.component.html',
  styleUrls: ['./tidings.component.css']
})
export class TidingsComponent implements OnInit {

  constructor(private message: MessageService) { }
  text: string;

  ngOnInit(): void {
  }

  sendMessage() {
    this.message.sendMessage().subscribe();
  }

  getMessage() {
    this.message.getMessage().subscribe(data => {
      console.log(data);
    });
  }

}
