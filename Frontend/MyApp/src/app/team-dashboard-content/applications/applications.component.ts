import { Component, OnInit } from '@angular/core';
import {ApplicationService} from '../../service/application/applicationService';
import {ApplicationDto} from '../../models/application/applicationDto';
import {UserProfileService} from '../../service/userProfile/userProfileService';
import {UserProfile} from '../../models/userProfile';
import {SkillsService} from '../../service/skills/skillsService';
import {Skills} from '../../models/skills';
import {NotificationService} from '../../service/notification/notification.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  constructor(private serviceApp: ApplicationService,
              private skillsService: SkillsService,
              private profileService: UserProfileService,
              private notification: NotificationService) { }

  isEmpty: boolean;
  applications: ApplicationDto[];
  userProfile: UserProfile = new UserProfile();
  skills: Skills[];
  invite: boolean;
  userApplication: boolean;

  ngOnInit(): void {
    this.userApplication = JSON.parse(localStorage.getItem('userApplication'));
    this.invite = JSON.parse(localStorage.getItem('invite'));
    if(!this.invite) {
      this.serviceApp.getAllTeamApplications().subscribe(data => {
        this.applications = data;
        this.isEmpty = this.applications === null;
      });
    } else {
      this.serviceApp.getAllTeamInvitations().subscribe(data => {
        this.applications = data;
        this.isEmpty = this.applications === null;
      });
    }
    localStorage.removeItem('invite');
  }

  getPersonalData(id: number) {
    this.profileService.getUserProfileById(id).subscribe( data => {
      this.userProfile = JSON.parse(data);
    });
  }

  getUserSkills(id: number) {
    this.skillsService.getAllUserSkillsById(id).subscribe( data => {
      this.skills = JSON.parse(data);
    });
  }

  applyApplication(application: ApplicationDto) {
    this.serviceApp.applyApplication(application).subscribe( x => {
      this.applications = this.applications.filter(obj => obj !== application);
      this.notification.displaySuccessNotification('A new team member has been added!')
    },
      error => this.notification.displayErrorNotification(error.error.message));
  }

  discardApplication(application: ApplicationDto) {
    this.serviceApp.discardApplication(application).subscribe( x => {
      this.applications = this.applications.filter(obj => obj !== application);
        this.notification.displaySuccessNotification('The application has been rejected!')
    },
      error => this.notification.displayErrorNotification(error.error.message));
  }

}
