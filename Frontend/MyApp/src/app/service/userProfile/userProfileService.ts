import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserProfile } from 'src/app/models/userProfile';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

    constructor(private httpClient: HttpClient) { }

    saveUserProfile(profile: UserProfile): Observable<UserProfile> {
      return this.httpClient.put<UserProfile>('http://localhost:8090/profile', profile);
      }

      getData(): Observable<UserProfile> {
        return this.httpClient.get<UserProfile>('http://localhost:8090/profile');
      }

      getUserProfileById(id: number) {
        return this.httpClient.get('http://localhost:8090/profile/'  + id, { responseType: 'text'});
    }
}
