import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private httpClient: HttpClient) {}

  sendMessage() {
    return this.httpClient.post('http://localhost:8090/message/send', 'test');
  }

  getMessage(): Observable<string> {
    return this.httpClient.get<string>('http://localhost:8090/message/receive');
  }

}
