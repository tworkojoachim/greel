import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { User } from '../auth/authentication.service';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegistrationService {


  constructor(private httpClient: HttpClient) { }

  headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
  public doRegistration(user: User) {
    return this.httpClient.post('http://localhost:8090/registration', user, {headers: this.headers});
  }
}
