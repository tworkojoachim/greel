import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private snackBar: MatSnackBar) { }

  displaySuccessNotification(message: string) {
    this.snackBar.open(message, 'X',{panelClass: 'notif-success'});
  }

  displayErrorNotification(message: string) {
    this.snackBar.open(message, 'X',{panelClass: 'notif-error'});
  }

  displayInfoNotification(message: string) {
    this.snackBar.open(message, 'X',{panelClass: 'notif-info'});
  }
}
