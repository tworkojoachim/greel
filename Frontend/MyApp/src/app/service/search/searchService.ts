import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Skills } from 'src/app/models/skills';
import {TeamSearchModel} from '../../models/search/teamSearchModel';
import {PeopleSearchModel} from '../../models/search/people/peopleSearchModel';
import {Observable} from 'rxjs';
import {UserProfile} from '../../models/userProfile';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private httpClient: HttpClient) { }

  getAllSearchTeams(search: TeamSearchModel) {
    return this.httpClient.post('http://localhost:8090/team/search', search, { responseType: 'text'});
  }

  getAllSearchPeople(search: PeopleSearchModel): Observable<UserProfile[]> {
    return this.httpClient.post<UserProfile[]>('http://localhost:8090/searchPeoples', search);
  }

}

