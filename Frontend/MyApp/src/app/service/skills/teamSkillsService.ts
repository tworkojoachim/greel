import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Skills } from 'src/app/models/skills';


@Injectable({
  providedIn: 'root'
})
export class TeamSkillsService {

  constructor(private httpClient: HttpClient) { }

  saveTeamSkill(skill: Skills) {
    return this.httpClient
      .post('http://localhost:8090/team/skill', skill);
  }

  getAllSkills() {
    return this.httpClient.get('http://localhost:8090/team/skills',  { responseType: 'text'});
  }

  getAllTeamSkillsById(id: number) {
    return this.httpClient.get('http://localhost:8090/team/skills/' + id,  { responseType: 'text'});
  }

  deleteSkill(id: number) {
    return this.httpClient.delete('http://localhost:8090/team/skills/' + id , { responseType: 'text'});
  }
}
