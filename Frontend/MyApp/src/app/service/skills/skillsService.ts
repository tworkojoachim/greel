import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Skills } from 'src/app/models/skills';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SkillsService {

    constructor(private httpClient: HttpClient) { }

    saveSkill(skill: Skills) {
        return this.httpClient
          .post('http://localhost:8090/skills/user', skill);
        }

    getAllSkills(): Observable<Skills[]> {
      return this.httpClient.get<Skills[]>('http://localhost:8090/skills/user');
    }

  getAllUserSkillsById(id: number) {
    return this.httpClient.get('http://localhost:8090/skills/user/' + id,  { responseType: 'text'});
  }

    deleteSkill(id: number) {
      return this.httpClient.delete('http://localhost:8090/skills/user/' + id , { responseType: 'text'});
    }
}
