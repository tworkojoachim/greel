import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Skills } from 'src/app/models/skills';


@Injectable({
  providedIn: 'root'
})
export class DataSkillsService {

  constructor(private httpClient: HttpClient) { }

  getAllSkillsByCategory(category: string) {
    return this.httpClient.get('http://localhost:8090/data/category/' + category, { responseType: 'text'});
  }

}
