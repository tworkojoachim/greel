import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';
import {NotificationService} from '../notification/notification.service';

export class User {
  constructor(
    public status?: string,
    public username?: string,
    public email?: string,
    public password?: string,
    public confirmPassword?: string,
  ) { }
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private notification: NotificationService) {
  }

  authenticate(username: string, password: any) {
    return this.httpClient.post<any>('http://localhost:8090/authorization', { username, password }).pipe(
      map(
        userData => {
          sessionStorage.setItem('username', username);
          const tokenStr = userData.token;
          const rToken = userData.refreshToken;
          sessionStorage.setItem('token', tokenStr);
          sessionStorage.setItem('refreshToken', rToken);
          return userData;
        }
      )
    );
  }

  isUserLoggedIn() {
    const user = sessionStorage.getItem('username');
    return !(user === null);
  }

  logOut() {
    return this.httpClient.post<any>('http://localhost:8090/loggingOut', sessionStorage.getItem('token'));
  }

  clearSession(message: string) {
    sessionStorage.clear();
    localStorage.clear();
    this.notification.displayInfoNotification(message);
    this.router.navigate(['login'])
  }

  refreshSession() {
    return this.httpClient.post<any>('http://localhost:8090/refreshToken', sessionStorage.getItem('refreshToken'));
  }
}

