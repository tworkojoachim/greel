import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {NotificationService} from '../notification/notification.service';

@Injectable({
  providedIn: 'root'
})
export class BasicAuthHttpInterceptorService implements HttpInterceptor {

  constructor(private router: Router,
              private auth: AuthenticationService,
              private notification: NotificationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler)  {
    if (sessionStorage.getItem('username') && sessionStorage.getItem('token')) {
      req = req.clone({
          headers: req.headers.set('Authorization', sessionStorage.getItem('token'))
      });
    }
    return next.handle(req).pipe( tap(() => {},
      (err: any) => {
      if(err instanceof HttpErrorResponse && err.status === 401 || err.status === 403) {
        if(sessionStorage.getItem('refreshToken')){
          this.auth.refreshSession().subscribe(data => {
            sessionStorage.setItem('token', data.token);
          }, error => this.logout())
        } else this.logout();
      }
      }))
  }

  logout() {
    this.auth.logOut().subscribe(data => {
      this.auth.clearSession('Session expired!');
    })
  }
}
