import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Team} from '../../models/team';
import {Observable} from 'rxjs';
import {UserProfile} from '../../models/userProfile';


@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private httpClient: HttpClient) {}

  getTeamId() {
    return this.httpClient.get('http://localhost:8090/team/id');
  }

  addTeam(team: Team) {
    return this.httpClient.post('http://localhost:8090/team', team, { responseType: 'text'});
  }

  haveTeam() {
    return this.httpClient
      .get('http://localhost:8090/team/existBy', { responseType: 'text'} );
  }

  isAdmin() {
    return this.httpClient
      .get('http://localhost:8090/team/admin', { responseType: 'text'} );
  }

  getTeam() {
    return this.httpClient
      .get('http://localhost:8090/team', { responseType: 'text'} );
  }

  getAllTeamUsers(): Observable<UserProfile[]> {
    return this.httpClient
      .get<UserProfile[]>('http://localhost:8090/team/users' );
  }

  getTeamById(id: number) {
    return this.httpClient
      .get('http://localhost:8090/team/' + id, { responseType: 'text'} );
  }

  removeUser(id: number) {
    return this.httpClient
      .delete('http://localhost:8090/team/remove/' + id, {responseType: 'text'} );
  }

  addPermission(id: number) {
    return this.httpClient
      .post('http://localhost:8090/team/permission/add/' + id, {responseType: 'text'} );
  }

  removePermission(id: number) {
    return this.httpClient
      .post('http://localhost:8090/team/permission/remove/' + id, {responseType: 'text'} );
  }

}
