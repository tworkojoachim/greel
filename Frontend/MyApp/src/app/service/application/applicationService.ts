import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Application} from '../../models/application/application';
import {ApplicationDto} from '../../models/application/applicationDto';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private httpClient: HttpClient) { }

  sendApplicationTeam(application: Application) {
    return this.httpClient.post('http://localhost:8090/application/team', application);
  }

  sendApplicationPeople(application: Application) {
    return this.httpClient.post('http://localhost:8090/application/people', application);
  }

  getAllTeamApplications(): Observable<ApplicationDto[]> {
    return this.httpClient.get<ApplicationDto[]>('http://localhost:8090/application');
  }

  getAllTeamInvitations(): Observable<ApplicationDto[]> {
    return this.httpClient.get<ApplicationDto[]>('http://localhost:8090/application/invite');
  }

  applyApplication(application: ApplicationDto) {
    return this.httpClient.post('http://localhost:8090/application/apply', application);
  }

  discardApplication(application: ApplicationDto) {
    return this.httpClient.post('http://localhost:8090/application/discard', application);
  }

  applyApplicationUser(application: ApplicationDto) {
    return this.httpClient.post('http://localhost:8090/application/applyUser', application);
  }

  discardApplicationUser(application: ApplicationDto) {
    return this.httpClient.post('http://localhost:8090/application/discardUser', application);
  }

  getAllUserApplications(): Observable<ApplicationDto[]> {
    return this.httpClient.get<ApplicationDto[]>('http://localhost:8090/application/personA');
  }

  getAllUserInvitations(): Observable<ApplicationDto[]> {
    return this.httpClient.get<ApplicationDto[]>('http://localhost:8090/application/personI');
  }

}
