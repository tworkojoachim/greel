export class PeopleSearchModel {
  constructor(
    public needsList?: string[],
    public category?: string,
    public experience?: number
  ) {}
}
