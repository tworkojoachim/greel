export class TeamSearchModel {
  constructor(
    public needsList?: string[],
    public category?: string,
    public sizeTeam?: number
  ) {}
}
