export class UserProfile {
    id: number;
    admin: boolean;
    constructor(
    public firstName?: string,
    public lastName?: string,
    public phoneNumber?: string,
    public gitHub?: string,
    public linkedin?: string,
    public position?: string,
    public socialStatus?: string,
    public experience?: number,
    ) {}
}

