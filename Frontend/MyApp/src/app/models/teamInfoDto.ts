export class TeamInfoDto {
  constructor(
    public name?: string,
    public description?: string,
    public projectDescription?: string,
    public peopleSought?: string,
    public teamSize?: number,
    public membersValue?: number,
    public dateOfCreation?: string,
  ) {}
}
