export class TeamSearch {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public peopleSought?: string,
    public teamSize?: number
  ) {}
}
