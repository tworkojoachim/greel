export class ApplicationDto {
  constructor(
    public id?: number,
    public message?: string,
    public date?: string,
    public user?: number,
    public team?: number,
    public userInvite?: boolean
  ) {}
}

