export class Team {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public projectDescription?: string,
    public peopleSought?: string,
    public teamSize?: number,
    public dateOfCreation?: string,
  ) {}
}
