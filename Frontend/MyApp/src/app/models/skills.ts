export class Skills {
    constructor(
    public id?: number,
    public name?: string,
    public numberOfMonth?: number,
    public level?: string,
    public category?: string
    ) {}
}

