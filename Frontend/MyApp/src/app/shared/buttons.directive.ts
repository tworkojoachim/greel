import { Directive, ElementRef, Renderer2, OnInit, HostListener} from '@angular/core';

@Directive({
  selector: '[appButtons]'
})
export class ButtonsDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

    @HostListener('mouseenter')
    mouseenter(eventDate: Event) {
    const a = this.el.nativeElement;
    this.renderer.setStyle(a, 'background-color', 'mediumslateblue');
    }

    @HostListener('mouseleave')
    mouseleave(eventDate: Event) {
    const a = this.el.nativeElement;
    this.renderer.removeStyle(a, 'background-color');
    }
}
