import { Component, OnInit } from '@angular/core';
import {SkillsService} from '../service/skills/skillsService';
import {DataSkillsService} from '../service/skills/dataSkillsService';
import {Skills} from '../models/skills';
import {DateSkills} from '../models/dateSkills';
import {NgForm} from '@angular/forms';
import {TeamSkillsService} from '../service/skills/teamSkillsService';
import {NotificationService} from '../service/notification/notification.service';

@Component({
  selector: 'app-team-needs',
  templateUrl: './team-needs.component.html',
  styleUrls: ['./team-needs.component.css']
})
export class TeamNeedsComponent implements OnInit {

  constructor(private teamSkillsService: TeamSkillsService,
              private dataSkills: DataSkillsService,
              private notification: NotificationService) { }

  skills: Skills = new Skills();
  category: string;

  skillList: Skills[];

  skillsName: DateSkills[];

  ngOnInit(): void {
    this.teamSkillsService.getAllSkills().subscribe( data => {
      this.skillList = JSON.parse(data);
    });
    this.category = 'BACKEND';
    this.dataSkills.getAllSkillsByCategory(this.category).subscribe( data => {
      this.skillsName = JSON.parse(data);
    });
  }

  saveSkill(form: NgForm) {
    this.skills.category = this.category;
    if(this.valid()) {
      this.teamSkillsService.saveTeamSkill(this.skills).subscribe(data => {
        this.notification.displaySuccessNotification('The skill has been added');
        this.skillList.push(data);
      }, error => {
        console.log(error);
        this.notification.displayErrorNotification(error.error.message);
      });
      form.resetForm();
    }
  }

  round(value: number) {
    return Math.round(value / 12 - 0.5);
  }

  deleteSkill(id: number) {
    this.teamSkillsService.deleteSkill(id).subscribe(() => this.notification.displaySuccessNotification('Deleted successfully!'));
    this.skillList = this.skillList.filter(item => item.id !== id);
  }

  newData($event: any) {
    this.dataSkills.getAllSkillsByCategory(this.category).subscribe( data => {
      this.skillsName = JSON.parse(data);
    });
  }

  valid() {
    return this.skills.category != null &&
      this.skills.name != null &&
      this.skills.level != null;
  }

}
