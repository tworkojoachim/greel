import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/auth/authentication.service';
import {NotificationService} from '../service/notification/notification.service';
import {UserProfileService} from '../service/userProfile/userProfileService';
import {SkillsService} from '../service/skills/skillsService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  message: string;

  constructor(private router: Router,
              private loginService: AuthenticationService,
              private notification: NotificationService,
              private profile: UserProfileService,
              private skills: SkillsService) { }

  ngOnInit() {
    if (this.loginService.isUserLoggedIn())
      this.router.navigate(['context']);
  }

  checkLogin() {
    this.loginService.authenticate(this.username, this.password).subscribe(
      data => {
        this.notification.displaySuccessNotification('Successfully login!');
        this.check();
        this.router.navigate(['context']);
      },
        error => {
        this.message = error.error.message;
        this.notification.displayErrorNotification('Invalid credentials!');
      });
  }

  check() {
    this.profile.getData().subscribe(data => {
      if(data.firstName != null)
        localStorage.setItem('isProfile', JSON.stringify(true));
    })
    this.skills.getAllSkills().subscribe(data => {
      if(data.length > 0)
        localStorage.setItem('isSkill', JSON.stringify(true));
    })
  }
}
