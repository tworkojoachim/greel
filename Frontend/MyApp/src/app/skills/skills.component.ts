import {Component, OnInit} from '@angular/core';
import {Skills} from '../models/skills';
import {SkillsService} from '../service/skills/skillsService';
import {NgForm} from '@angular/forms';
import {DataSkillsService} from '../service/skills/dataSkillsService';
import {DateSkills} from '../models/dateSkills';
import {NotificationService} from '../service/notification/notification.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {

  constructor(private skillService: SkillsService,
              private dataSkills: DataSkillsService,
              private notifications: NotificationService) { }

  skills: Skills = new Skills();
  category: string;

  skillList: Skills[];

  skillsName: DateSkills[];

  ngOnInit(): void {
    this.skillService.getAllSkills().subscribe( data => {
      this.skillList = data;
    });
    this.category = 'BACKEND';
    this.dataSkills.getAllSkillsByCategory(this.category).subscribe( data => {
      this.skillsName = JSON.parse(data);
      });
  }

  saveSkill(form: NgForm) {
    this.skills.category = this.category;
    if(this.valid()) {
      this.skillService.saveSkill(this.skills).subscribe(data => {
        this.notifications.displaySuccessNotification('The skill has been added');
        this.skillList.push(data);
        localStorage.setItem('isSkill', JSON.stringify(true));
      }, error => {
        this.notifications.displayErrorNotification(error.error.message);
      });
      form.resetForm();
    }
  }

  round(value: number) {
    const x = Math.round(value / 12 - 0.5);
    if(x === 0) return ''
    else return x.toString() + ' years';
  }

  deleteSkill(id: number) {
    this.skillService.deleteSkill(id).subscribe(() => {
      this.notifications.displaySuccessNotification('Deleted successfully!');
      this.skillList = this.skillList.filter(item => item.id !== id);
      if(this.skillList.length === 0)
        localStorage.setItem('isSkill', JSON.stringify(false));
    });
  }

  newData($event: any) {
    this.dataSkills.getAllSkillsByCategory(this.category).subscribe( data => {
      this.skillsName = JSON.parse(data);
    });
  }

  valid() {
    return this.skills.category != null &&
      this.skills.name != null &&
      this.skills.level != null &&
      this.skills.numberOfMonth != null;
  }

}
