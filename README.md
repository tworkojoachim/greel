# Greel

* [Project description](#project-description)
* [Technologies](#technologies)
* [Tools](#tools)
* [Images](#images)

## Project description

The Greel project was created for people who want to gain first experience in a real team with real tasks and challenges.
Each of you can join this interesting initiative for free.
Greel's possibilities are really extensive.
We have the opportunity to set up our own dream team and implement a project in which we will have a real impact on its appearance.
If we have no idea, we can join already existing teams, all we need is that our skills overlap with the required ones and we send the application.
There are no hard goals or guidelines. The most important slogan is science.
We have an amazing opportunity to develop with more or less experienced developers.
The experience gained in the implementation of the project can be entered into CV, which will help to find your dream job faster and speed up the carousel in the IT industry.

## Technologies

* Java 11
* Spring Boot 2.2.6
* Spring Data
* Spring Security
* Lombok
* PostgreSQL
* Docker
* Typescript
* Angular 9
* Bootstrap

## Tools

* IntelliJ IDEA
* WebStorm
* DataGrip
* Docker desktop
* Postman

## Images

### User profile

![Profile window](images/profile.png)

<br />

### User skills

![Skills window](images/skills.png)

<br />

### Search window

![Search window](images/search_1.png)

<br />

### Search team

![Search team window](images/search_2.png)

<br />

### Team information

![Team information window](images/three.png)

<br />

### Team needs

![Team needs window](images/team_needs.png)

<br />

### Team applications

![Team applications window](images/team_applications.png)

<br />

### Team members

![Team members window](images/team_members.png)

<br />